//app.js
var config = require('./config')
var util = require('./utils/util')
var audio = require('./utils/audio')
var socket = require('./utils/socket')
var auth = require('./utils/auth')

let baseUrl = config.service.host + '/html'
let cdnBaseUrl = config.service.cdnHost + '/html'

var count = 0
function heartBeat() {  // per 500ms
  ++count
  let page = util.getPage()
  if(page && page.data.loaded && page.data.ready
    && page.onHeartBeat && typeof(page.onHeartBeat) == 'function') {
    page.onHeartBeat(count)
  }
}


App({
  globalData: {
    baseUrl,
    customer: config.customer,
    cdnBaseUrl,
    resRoot: config.service.resourceUrl + '/',
    fallbackAvatarUrl: "/pages/index/user-unlogin.png",
    fallbackAvatarUrlMan: baseUrl + "/man.jpg",
    fallbackAvatarUrlWoman: baseUrl + "/women.jpg",
    fallbackEmblem: baseUrl + "/org.png",
    userRoles: config.consts.USER_ROLES,
    urlMap: {
      doc: '/pages/content/article',
      article: '/pages/content/article',
      activity: '/pages/content/activity',
      meeting: '/pages/content/meeting',
      module: '/pages/list/module',
      album: '/pages/content/photo',
    },
    speakerId: [1,5,6,7],
    speakers: ['普通话男声', '静琪女声', '欢馨女声', '碧萱女声'],
  },
  onMinuteChange: function () {
    let delta = 60000 - (Date.now() % 60000)
    setTimeout(this.onMinuteChange.bind(this), delta)
    util.log.onMinuteChange(delta)
    let page = util.getPage()
    if(page && page.data.loaded && page.data.ready
      && page.onMinuteChange && typeof(page.onMinuteChange) == 'function') {
      page.onMinuteChange()
    }
  },
  onLaunch: function () {
    wx.hideTabBar()
    //setTimeout(this.login.bind(this), 0);
    //setTimeout(this.newLogin.bind(this), 0);
    setTimeout(auth.login, 0)
    setInterval(heartBeat, 500)
    setTimeout(this.onMinuteChange.bind(this), 0)
    setTimeout(audio.init.bind(audio), 0) // use setTimeout to get getApp() working
  },
})
