/**
 * 小程序配置文件
 */

// 此处主机域名修改成腾讯云解决方案分配的域名
//var host = 'https://tgf0lubh.qcloud.la';
//var host = 'http://192.168.0.108:5757';
//const root = 'http://192.168.0.108:5757', path = ''
//const root = 'https://dj.cyberytech.com', path = '/party-dev'
const root = 'https://dj.cyberytech.com', path = '/party'
const cdnRoot = 'http://dj.cdn.cyberytech.com'
var host = root + path
var cdnHost = cdnRoot + path
const customer = require('./customer')

var config = {

  appid: customer.appId,
  customer,
  //debug: true,                              // just skip animation
  //debug: {url: '/pages/list/album?debug=1'},
  //debug: {url: '/pages/content/photo?debug=1&id=105&currentId=0&showList=0'},

  // 下面的地址配合云端 Demo 工作
  service: {
    host,
    cdnHost,

    socketUrl: root,
    socketPrefix: path,
    apiUrl: `${host}/weapp`,
    resourceUrl: `${host}/weapp/resource`,
    documentUrl: `${host}/weapp/document`,
    commentUrl: `${host}/weapp/comments`,
    pageUrl: `${host}/weapp/page`,

    // 登录地址，用于建立会话
    loginUrl: `${host}/weapp/login`,

    // 测试的请求地址，用于测试会话
    requestUrl: `${host}/weapp/user`,

    // 测试的信道服务地址
    tunnelUrl: `${host}/weapp/tunnel`,

    // 上传图片接口
    uploadUrl: `${host}/weapp/upload`
  },
  default: {
    iconUrl: `${host}/html/eletech.png`,
  },
  consts: {
    USER_ROLES: {
      BANNED: 0,
      NEW: 1,
      INREVIEW: 2,
      REJECTED: 3,
      NORMAL: 4,
    },
    ABSTRACT_LENGTH: 10,
    WX_HEADER_SKEY: 'X-WX-Skey',
    WX_HEADER_CODE: 'X-WX-Code',
    WX_SESSION_MAGIC_ID: 'F2C224D4-2BCE-4C64-AF9F-A6D872000D1A',
  }
};

module.exports = config;
