var app = getApp()
var util = require('../../utils/util')
var audio = require('../../utils/audio')
var config = require('../../config')
var wxParse = require('../../utils/wxParse')
var api = require('../../utils/webApi')
const AUDIO_FILES_FIRST_LOAD = 2
Page({
  data: {
    foldComments: true,
    loaded: false,
    textReply: true,
    commentType: 'new',
    reply: '',
    inputHeight: 60,
    inputLine: 60,
    inputFont: 40,
    inputValue: '',
    page: {
      bottomPanel: {height: 120}
    },
  },
  commentInput: function(event) {
    this.setData({reply:event.detail.value})
  },
  adjustInput: function(lineCount) {
    let inputHeight = Math.max(1, lineCount) * this.data.inputLine
    this.setData({
      inputHeight,
      'page.bottomPanel': {height: Math.max(inputHeight+20+20, 120)}
    })
    util.adjustPageLayout()
  },
  commentResize: function(event) {
    console.log(event)
    this.adjustInput(event.detail.lineCount)
  },
  commentFocus: function(event) {
    this.setData({replying: true})
  },
  commentBlur: function(event) {
    this.setData({replying: false})
  },
  endRecord: function(event) {
    console.log('endRecord',event)
    wx.hideToast()
    wx.getRecorderManager().stop()
  },
  record: function(event) {
    wx.showToast({
      title: '',
      //icon: 'success',
      image: '/icon/record.png',
      duration: 60000,
    })
    console.log(event)
    const recorderManager = wx.getRecorderManager()
    recorderManager.onStart(() => {
      console.log('recorder start')
    })
    recorderManager.onStop((res) => {
      console.log('recorder stop', res)
      const { tempFilePath } = res
      console.log(tempFilePath)
      api.upload(tempFilePath)
        .then(api.log)
        .then(res=>{
          if(res.success) {
            this.setData({
              voice: res.data, 
              reply: '',
              inputValue: '',
            })
            this.sendReply()
          }
        })
        .catch(api.log)
        .then(this.clearComment.bind(this))
    })
    const options = {
      duration: 60000,
      sampleRate: 8000,
      numberOfChannels: 1,
      encodeBitRate: 32000,
      //format: 'aac',
      format: 'mp3',
    }
    recorderManager.start(options)
  },
  toggleReply: function() {
    this.setData({textReply: !this.data.textReply})
  },

  refreshComment: function(idx, data) {
    let update = {...this.data.commentList[idx], ...data}
    update.displayTime = util.showOldDate(update.createdAt)
    this.setData({['commentList['+idx+']']: update})
  },
  refreshComments: function() {
    for(const i in this.data.commentList) {
      this.refreshComment(i)
    }
  },
  onReady: function() {
    if(!this.data.loaded) {
      wx.showLoading({title:'加载中'})
    } else {
      this.setData({ready: true})
      this.onShow()
    }
  },
  onShow: function() {
    if(this.data.ready && this.data.loaded) {
      audio.register()
      if(this.data.title) {
        this.setData({titleText: this.data.title}, ()=>util.updatePage(this))
      } else {
        util.updatePage(this)
      }
    }
  },
  init: function() {
    // 取文章
    api.requireDoc(this.data.id)
      .then(util.log.requireDoc)
      .then(d=>this.setData(d))
      .then(()=>this.setData({titleText: this.data.title}))
      .then(()=>wxParse.wxParse('content', 'html', this.data.content||"", this))
    // 取评论
      .then(()=>api.requireCom(this.data.id))
      .then(d=>this.setData({commentList: d}))
      .then(()=>this.refreshComments())
    // 处理日期
      .then(()=>this.updateStatus())
  },
  updateStatus: function() {
    let now = new Date()
    let startTime = new Date(this.data.startTime)
    let endTime = new Date(this.data.endTime)
    let deadlineTime = this.data.deadlineTime 
      ? new Date(this.data.deadlineTime)
      : new Date('2100-01-01')
    let participants = this.data.participants.filter(participant=>participant.status == 'SCHEDULED')
    let members = this.data.attender.length + participants.length
    let cells = Math.floor((members + 1) / 2)
    this.setData({
      now, startTime, endTime, deadlineTime,
      //canComment: now < deadlineTime,
      canComment: true,
      canRegister: now - 30*60*1000 > startTime && now < endTime,
      displayTime: util.getCnTime(startTime),
      deadlineDate: util.getCnTime(deadlineTime),
      readDate: util.getCnDate(new Date(this.data.readTime)),
      readTime: util.getCnTime(new Date(this.data.readTime)),
      participants,
      memberListHeight: members > 5 ? 400 : 200,
      memberListWidth: Math.max(750, cells*150),
    })
  },
  signup: function() {
    console.log(`signup: pending=${this.data.pending}`)
    if(this.data.pending) return
    this.setData({pending:true})
    api.post({
      url: '/document/'+this.data.id+'/signup',
    })
    .then(api.log)
    .then(res=>{
      if(res.success) {
        this.setData({
          ['participants['+this.data.participants.length+']']: app.globalData.userInfo.appUser.name,
          myOrder: this.data.participants.length,
          status: 'SCHEDULED',
        })
        wx.showToast({
          title: '报名成功',
          icon: 'success',
          duration: 2000,
        })
      }
    })
    .catch()
    .then(()=>{this.setData({pending:false})})
  },
  cancel: function() {
    console.log(`cancel: pending=${this.data.pending}`)
    if(this.data.pending) return
    this.setData({pending:true})
    api.post({
      url: '/document/'+this.data.id+'/cancel',
    })
    .then(api.log)
    .then(res=>{
      if(res.success) {
        this.setData({
          participants: this.data.participants.filter((item,index)=>index!=this.data.myOrder),
          status: null,
          myOrder: null,
        })
        wx.showToast({
          title: '取消报名成功',
          icon: 'success',
          duration: 2000,
        })
      }
    })
    .catch()
    .then(()=>{this.setData({pending:false})})
  },
  postCode: function(code) {
    return api.post(
      {
        url: '/code',
        data: {path: code.path.split('=')[1]}
      })
      .then(util.log.postCode)
  },
  tapScan: function() {
    return util.scanCode('pages/index/index?scene=activity-'+this.data.id)
      .then(util.log.scanCode)
      .then(this.postCode.bind(this))
      .then(res=>util.showModal({content: res.message}))
      .then(()=>this.setData({status: 'PRESENT'}))
      .catch(util.error.postCode)
      .catch(e=>util.showModal({content:'扫码失败：' + e,}))
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    // 保存调用参数
    Promise.resolve(params)
      .then(util.log.params)
      .then(d=>this.setData(d))
      .then(()=>this.init())
      .then(()=>this.setData({loaded:true}))
      .then(wx.hideLoading)
      .then(this.onShow)
  },
  markAsRead: function() {
    if(this.data.canRead) {
      api.post({url: '/document/'+this.data.id+'/receipt'})
        .then(api.log)
        .then(res=>{
          if(res.success) {
            this.setData({
              canRead: false,
              ["viewer.["+this.data.viewer.length+"]"]: {
                auth: app.globalData.userInfo.name,
                avatarUrl: app.globalData.userInfo.avatarUrl,
                id: app.globalData.userInfo.appUser.userInfo.id,
                name: app.globalData.userInfo.appUser.name,
                gender: app.globalData.userInfo.appUser.userInfo.gender,
              }
            })
            wx.showToast({
              title: res.data.message,
              icon: 'success',
              duration: 2000,
            })
          }
        })
    }
  },
  toggleFoldComments: function() {
    this.setData({foldComments: !this.data.foldComments})
  },
  getDisplayTime: function(timeStr) {
  },
  longpressComment: function(event) {
    console.log(event)
  },
  tapCommentReply: function(event) {
    console.log(event)
    this.setData({
      commentType: 'reply',
      commentIdx: event.target.id,
      replying: true,
    })
  },
  tapCommentEdit: function(event) {
    this.setData({
      commentType: 'edit',
      commentIdx: event.target.id,
      reply: this.data.commentList[event.target.id].content,
      inputValue: this.data.commentList[event.target.id].content,
      replying: true,
    })
  },
  tapCommentTitleBar: function(event) {
    let id = event.currentTarget.id
    this.setData({['commentList['+id+'].showContent']: !this.data.commentList[parseInt(id)].showContent})
  },
  playComment: function(event) {
    let id = parseInt(event.target.id)
    let comment = this.data.commentList[id]
    console.log(comment)
    let ctx = wx.createInnerAudioContext()
    ctx.autoplay = true
    ctx.onEnded(()=>{
      console.log('comment end')
      this.setData({['commentList['+id+'].playing']: false})
    })
    this.setData({['commentList['+id+'].playing']: true})
    ctx.onError(e=>{console.log('audio error:',e)})
    ctx.src = app.globalData.resRoot + comment.resId
  },
  sendReply: function(event) {
    if(!this.data.commentPending) {
      let contentType
      if(this.data.voice) {
        contentType = 'audio'
      } else if(event && event.detail && event.detail.value && event.detail.value.input) {
        contentType = 'text'
      } else {
        return
      }
      this.setData({commentPending: true})
      let data = {
        type: this.data.commentType,
        content: contentType == 'text' ? event.detail.value.input : null,
        contentType,
        resId: contentType == 'audio' ? this.data.voice : null,
      }
      if(this.data.commentType !== 'new') {
        let id = this.data.commentList[this.data.commentIdx].id
        if(this.data.commentType === 'edit') {
          data.id = id
        } else if(this.data.commentType === 'reply') {
          data.refId = id
        }
      }
      api.post({
        url: '/document/'+this.data.id+'/comment',
        data,
      })
      .then(api.log)
      .then(res=>{
        if(res.success) {
          if(this.data.commentType === 'edit') {
            this.refreshComment(this.data.commentIdx, {content:data.content})
          } else {
            this.setData({
              ['commentList['+this.data.commentList.length+']']: res.data,
              commentCount: this.data.commentCount + 1,
              commentUserCount: this.data.commentUserCount + (this.data.myReplies ? 0 : 1),
              myReplies: this.data.myReplies + 1,
            })
            this.refreshComment(this.data.commentList.length-1)
          }
        }
      })
      .catch(api.log) //ignore
      .then(this.clearComment.bind(this))
    }
  },
  clearComment: function() {
    this.setData({
      reply: '',
      inputValue: '',
      replying: false,
      commentPending: false,
      commentType: 'new',
      typing: false,
    })
    this.adjustInput(1)
  },
  onReachBottom: function(event) {
    console.log('onReachBottom', event)
    if(this.data.autoRead) {
      this.markAsRead()
    }
  },
})
