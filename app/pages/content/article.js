var app = getApp()
var util = require('../../utils/util')
var audio = require('../../utils/audio')
var config = require('../../config')
var wxParse = require('../../utils/wxParse')
var api = require('../../utils/webApi')
const AUDIO_FILES_FIRST_LOAD = 2
Page({
  data: {
    foldComments: true,
    loaded: false,
    textReply: true,
    commentType: 'new',
    reply: '',
    inputHeight: 60,
    inputLine: 60,
    inputFont: 40,
    inputValue: '',
    page: {
      bottomPanel: {height: 120}
    },
  },
  commentInput: function(event) {
    this.setData({reply:event.detail.value})
  },
  adjustInput: function(lineCount) {
    let inputHeight = Math.max(1, lineCount) * this.data.inputLine
    this.setData({
      inputHeight,
      'page.bottomPanel': {height: Math.max(inputHeight+20+20, 120)}
    })
    util.adjustPageLayout()
  },
  commentResize: function(event) {
    console.log(event)
    this.adjustInput(event.detail.lineCount)
  },
  commentFocus: function(event) {
    this.setData({replying: true})
  },
  commentBlur: function(event) {
    this.setData({replying: false})
  },
  audioHandler: function(event) {
    console.log('audioHandler',event.currentTarget.dataset.method,event.target.id)
    if(event.currentTarget.dataset.method == 'switchSpeaker') {
      audio.switchSpeaker(parseInt(event.detail.value))
    } else {
      audio[event.currentTarget.dataset.method](event.currentTarget.id)
    }
  },
  endRecord: function(event) {
    console.log('endRecord',event)
    wx.hideToast()
    wx.getRecorderManager().stop()
  },
  record: function(event) {
    wx.showToast({
      title: '',
      //icon: 'success',
      image: '/icon/record.png',
      duration: 60000,
    })
    console.log(event)
    const recorderManager = wx.getRecorderManager()
    recorderManager.onStart(() => {
      console.log('recorder start')
    })
    recorderManager.onStop((res) => {
      console.log('recorder stop', res)
      const { tempFilePath } = res
      console.log(tempFilePath)
      api.upload(tempFilePath)
        .then(api.log)
        .then(res=>{
          if(res.success) {
            this.setData({
              voice: res.data, 
              reply: '',
              inputValue: '',
            })
            this.sendReply()
          }
        })
        .catch(api.log)
        .then(this.clearComment.bind(this))
    })
    const options = {
      duration: 60000,
      sampleRate: 8000,
      numberOfChannels: 1,
      encodeBitRate: 32000,
      //format: 'aac',
      format: 'mp3',
    }
    recorderManager.start(options)
  },
  toggleReply: function() {
    this.setData({textReply: !this.data.textReply})
  },

  refreshComment: function(idx, data) {
    let update = {...this.data.commentList[idx], ...data}
    update.displayTime = util.showOldDate(update.createdAt)
    this.setData({['commentList['+idx+']']: update})
  },
  refreshComments: function() {
    for(const i in this.data.commentList) {
      this.refreshComment(i)
    }
  },
  articleInit: function() {
    let statusText = ''
    if(this.data.status == 'read') statusText = '已读'
    this.setData({
      statusText,
      autoRead: true,
      canRead: this.data.published && !this.data.status && this.data.now < this.data.deadlineTime,
    })
  },
  onReady: function() {
    if(!this.data.loaded) {
      wx.showLoading({title:'加载中'})
    }
    this.setData({ready: true})
    this.onShow()
  },
  meetingInit: function() {
    let now = new Date()
    let startTime = new Date(this.data.startTime)
    let endTime = new Date(this.data.endTime)
    let members = this.data.attender.length + this.data.vacant.length + this.data.viewer.length + this.data.scheduled.length
    let cells = Math.floor((members + 1) / 2)
    let options = {
      now, startTime, endTime,
      autoRead: true,
      displayDate: util.getCnDate(startTime),
      displayTime: util.getCnTime(startTime),
      readDate: util.getCnDate(new Date(this.data.readTime)),
      readTime: util.getCnTime(new Date(this.data.readTime)),
      canComment: false,
      canRead: false,
      memberListHeight: members > 5 ? 400 : 200,
      memberListWidth: Math.max(750, cells*150),
    }
    if(now > this.data.deadlineTime) {
      options.statusText = '回复已截止'
    } else if(!this.data.status) {
      options.statusText = '未受邀参会'
    } else {
      if(this.data.status == 'SCHEDULED') {
        options.canVacant = now < startTime
        options.canRegister = now > startTime - 30*60*1000 && now < endTime
        options.canRead = this.data.published && now > endTime
      }
      if(this.data.published && now > this.data.endTime) {
        options.canComment = true
      } else if(now < startTime) {
        options.statusText = '会议未开始'
      } else if(now < endTime) {
        options.statusText = '会议进行中'
      } else {
        options.statusText = '会议记录整理中'
      }
    }
    util.log.mi(options)
    this.setData(options)
  },
  activityInit: function() {
    let now = new Date()
    let startTime = new Date(this.data.startTime)
    let endTime = new Date(this.data.endTime)
    let deadlineTime = new Date(this.data.deadlineTime)
    this.setData({
      now, startTime, endTime, deadlineTime,
      displayTime: util.getCnTime(startTime),
      deadlineDate: util.getCnTime(deadlineTime),
      readDate: util.getCnDate(new Date(this.data.readTime)),
      readTime: util.getCnTime(new Date(this.data.readTime)),
    })
  },
  taskInit: function() {
  },
  onUnload: function() {
  },
  onShow: function() {
    if(this.data.ready && this.data.loaded) {
      audio.paint()
      if(this.data.title) {
        this.setData({titleText: this.data.title})
      }
      util.updatePage()
    }
  },
  onAudioEnd: function() {
    let {currentAudio, currentSpeaker, audioFiles} = this.data
    ++currentAudio
    if(currentAudio >= audioFiles) {
      this.markAsRead()
      this.setData({
        currentAudio: 0,
        inPlay: false,
        playing: false,
      })
      return
    }
    this.data.audioList[currentSpeaker][currentAudio].play()
    this.setData({currentAudio})
    let len = this.data.audioList[currentSpeaker].length
    if(len < audioFiles) {
      let audio = wx.createInnerAudioContext()
      audio.src = `${app.globalData.baseUrl}/audio/${this.data.id}-${len+1}-${this.data.speakerId[currentSpeaker]}.mp3`
      audio.onEnded(this.onAudioEnd)
      this.setData({[`audioList[${currentSpeaker}][${len}]`]: audio})
    }
  },
  toggleAudio: function() {
    if(this.data.playing) {
      this.data.audioList[this.data.currentSpeaker][this.data.currentAudio].pause()
    } else {
      this.data.audioList[this.data.currentSpeaker][this.data.currentAudio].play()
      this.setData({inPlay:true})
    }
    this.setData({playing: !this.data.playing})
  },
  init: function() {
    audio.paint()
    let now = new Date()
    let deadlineTime = this.data.deadlineTime 
      ? new Date(this.data.deadlineTime)
      : new Date('2100-01-01')
    this.setData({
      now, deadlineTime,
      canComment: now < deadlineTime,
      displayTime: util.getCnDate(new Date(this.data.createdAt))
    })

    switch(this.data.type) {
      case 'article': return this.articleInit()
      case 'meeting': return this.meetingInit()
      case 'activity': return this.activityInit()
      case 'task': return this.taskInit()
      default: console.log('unknown type', this.data.type)
    }
  },
  signup: function() {
    console.log(`signup: pending=${this.data.pending}`)
    if(this.data.pending) return
    this.setData({pending:true})
    api.post({
      url: '/document/'+this.data.id+'/signup',
    })
    .then(api.log)
    .then(res=>{
      if(res.success) {
        this.setData({
          ['participants['+this.data.participants.length+']']: app.globalData.userInfo.appUser.name,
          myOrder: this.data.participants.length,
          status: 'SCHEDULED',
        })
        wx.showToast({
          title: '报名成功',
          icon: 'success',
          duration: 2000,
        })
      }
    })
    .catch()
    .then(()=>{this.setData({pending:false})})
  },
  cancel: function() {
    console.log(`cancel: pending=${this.data.pending}`)
    if(this.data.pending) return
    this.setData({pending:true})
    api.post({
      url: '/document/'+this.data.id+'/cancel',
    })
    .then(api.log)
    .then(res=>{
      if(res.success) {
        this.setData({
          participants: this.data.participants.filter((item,index)=>index!=this.data.myOrder),
          status: null,
          myOrder: null,
        })
        wx.showToast({
          title: '取消报名成功',
          icon: 'success',
          duration: 2000,
        })
      }
    })
    .catch()
    .then(()=>{this.setData({pending:false})})
  },
  submitVacant: function(event) {
    if(event.detail.value.reason) {
      this.setData({showVacantPanel:false})
      api.post({
        url: '/document/'+this.data.id+'/vacant',
        data: {
          reason: event.detail.value.reason,
        }
      })
      .then(res=>{
        if(res.success) {
          this.setData({
            status: 'VACANT',
            readDate: util.getCnDate(new Date()),
            canVacant: false,
            canRead: false,
            canRegister: false,
            statusText: '已请假',
            ['vacant['+this.data.vacant.length+']']: {
              name: app.globalData.userInfo.appUser.name,
              reason: event.detail.value.reason,
              avatarUrl: app.globalData.userInfo.avatarUrl,
            },
          })
          wx.showToast({
            title: '请假成功',
            icon: 'success',
            duration: 2000,
          })
        }
      })
    }
  },
  toggleVacantPanel: function() {
    this.setData({showVacantPanel:!this.data.showVacantPanel})
  },
  tapScan: function() {
    app.globalData.doScan = true
    //app.globalData.doScanThen = ()=>{} // TODO: rebuild page stack ? 
    wx.switchTab({url: '/pages/tab/meeting'})
  },
  endPageLoading: function() {
    wx.hideLoading()
    this.setData({
      loaded:true
    })
    this.onShow()
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    // 保存调用参数
    Promise.resolve(params)
      .then(api.log)
      .then(d=>this.setData(d))
    // 取文章
      .then(()=>api.requireDoc(params.id))
      .then(api.log)
      .then(d=>this.setData(d))
      .then(()=>this.setData({titleText: this.data.title}))
      .then(()=>wxParse.wxParse('content', 'html', this.data.content||"", this))
    // 取评论
      .then(()=>api.requireCom(params.id))
      .then(d=>this.setData({commentList: d}))
      .then(this.refreshComments)
      .then(this.init)
    // 显示页面
      .then(()=>console.log(this.data))
      .then(this.endPageLoading)
  },
  markAsRead: function() {
    if(this.data.canRead) {
      api.post({url: '/document/'+this.data.id+'/receipt'})
        .then(api.log)
        .then(res=>{
          if(res.success) {
            this.setData({
              canRead: false,
              ["viewer.["+this.data.viewer.length+"]"]: {
                auth: app.globalData.userInfo.name,
                avatarUrl: app.globalData.userInfo.avatarUrl,
                id: app.globalData.userInfo.appUser.id,
                name: app.globalData.userInfo.appUser.name,
                gender: app.globalData.userInfo.appUser.gender,
              }
            })
            wx.showToast({
              title: res.data.message,
              icon: 'success',
              duration: 2000,
            })
          }
        })
    }
  },
  toggleFoldComments: function() {
    this.setData({foldComments: !this.data.foldComments})
  },
  getDisplayTime: function(timeStr) {
  },
  longpressComment: function(event) {
    console.log(event)
  },
  tapCommentReply: function(event) {
    console.log(event)
    this.setData({
      commentType: 'reply',
      commentIdx: event.target.id,
      replying: true,
    })
  },
  tapCommentEdit: function(event) {
    this.setData({
      commentType: 'edit',
      commentIdx: event.target.id,
      reply: this.data.commentList[event.target.id].content,
      inputValue: this.data.commentList[event.target.id].content,
      replying: true,
    })
  },
  tapCommentTitleBar: function(event) {
    let id = event.currentTarget.id
    this.setData({['commentList['+id+'].showContent']: !this.data.commentList[parseInt(id)].showContent})
  },
  playComment: function(event) {
    let id = parseInt(event.target.id)
    let comment = this.data.commentList[id]
    console.log(comment)
    let ctx = wx.createInnerAudioContext()
    ctx.autoplay = true
    ctx.onEnded(()=>{
      console.log('comment end')
      this.setData({['commentList['+id+'].playing']: false})
    })
    this.setData({['commentList['+id+'].playing']: true})
    ctx.onError(e=>{console.log('audio error:',e)})
    ctx.src = app.globalData.resRoot + comment.resId
  },
  sendReply: function(event) {
    if(!this.data.commentPending) {
      let contentType
      if(this.data.voice) {
        contentType = 'audio'
      } else if(event && event.detail && event.detail.value && event.detail.value.input) {
        contentType = 'text'
      } else {
        return
      }
      this.setData({commentPending: true})
      let data = {
        type: this.data.commentType,
        content: contentType == 'text' ? event.detail.value.input : null,
        contentType,
        resId: contentType == 'audio' ? this.data.voice : null,
      }
      if(this.data.commentType !== 'new') {
        let id = this.data.commentList[this.data.commentIdx].id
        if(this.data.commentType === 'edit') {
          data.id = id
        } else if(this.data.commentType === 'reply') {
          data.refId = id
        }
      }
      api.post({
        url: '/document/'+this.data.id+'/comment',
        data,
      })
      .then(api.log)
      .then(res=>{
        if(res.success) {
          if(this.data.commentType === 'edit') {
            this.refreshComment(this.data.commentIdx, {content:data.content})
          } else {
            this.setData({
              ['commentList['+this.data.commentList.length+']']: res.data,
              commentCount: this.data.commentCount + 1,
              commentUserCount: this.data.commentUserCount + (this.data.myReplies ? 0 : 1),
              myReplies: this.data.myReplies + 1,
            })
            this.refreshComment(this.data.commentList.length-1)
          }
        }
      })
      .catch(api.log) //ignore
      .then(this.clearComment.bind(this))
    }
  },
  clearComment: function() {
    this.setData({
      reply: '',
      inputValue: '',
      replying: false,
      commentPending: false,
      commentType: 'new',
      typing: false,
    })
    this.adjustInput(1)
  },
  changeSpeaker: function(event) {
    this.setData({currentSpeaker: event.detail.value})
  },
  onReachBottom: function(event) {
    console.log('onReachBottom', event)
    if(this.data.autoRead) {
      this.markAsRead()
    }
  },
})
