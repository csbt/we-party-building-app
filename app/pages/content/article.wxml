<import src="/utils/wxParse.wxml"/>
<import src="/utils/templates.wxml"/>
<import src="/utils/audio.wxml"/>
<view class="page" wx:if="{{loaded}}"
  style="{{type=='meeting'?'background-color:white;':''}}"
  >
  <template is="audio-new" data="{{globalData,...audio,height:page.audio.height}}"/>
  <scroll-view class="page-article"
    scroll-y="{{true}}"
    bindscrolltolower="onReachBottom"
    style="height:{{listHeight}}px;{{style}}">
    <!-- TODO: 如果已读，添加跳转到底部的按钮和回复按钮 -->
    <view class="center" style="font-weight:bold;margin-top:10rpx;font-size:120%">
      <text>{{title}}</text>
      <block wx:if="{{audio.canAdd}}">
        <text
          bindtap="audioHandler" data-method="playThis"
          >📢</text>
        <text
          wx:if="{{audio.playList.length}}"
          bindtap="audioHandler" data-method="addThis"
          style="margin-left:.5em"
          >➕</text>
      </block>
    </view>
    <view class="weui-flex text70 top20">
      <view>{{displayTime}}</view>
      <block wx:if="{{source}}">
        <text decode="{{true}}">&nbsp;&nbsp;来源&nbsp;</text>
        <view>{{source}}</view>
      </block>
      <view class="weui-flex__item"/>
      <view>👁</view>
      <text decode="{{true}}">&nbsp;&nbsp;{{viewCount}}</text>
    </view>
    <view class="div-hor__full" style="margin-top:10rpx;margin-bottom:20rpx"/>
    <block wx:if="{{published}}">
      <!-- 正文 -->
      <template is="wxParse" data="{{wxParseData:content.nodes}}"/>
      <!-- 标记已读按钮 -->
      <block wx:if="{{canRead}}">
        <button class="weui-btn" type="primary" bindtap="markAsRead"
          >标记已读（+{{readBonus}}分）</button>
      </block>
      <!-- 评论 -->
      <block wx:else>  <!-- 标记已读后再显示评论 -->
        <view style="height:100rpx"/>
        <!-- 统计信息 -->
        <view class="article_viewer"
          wx:if="{{viewer.length}}"
          >
          <view
            style="font-size:80%;color:grey"
            >
            <text>{{viewer[0].name}}等{{viewer.length}}人阅读了此文</text>
            <text wx:if="{{readBonus}}">，各获得{{readBonus}}积分</text>
          </view>
          <scroll-view class="viewer"
            scroll-x="{{true}}"
            >
            <block wx:for="{{viewer}}" wx:key="id">
              <view style="min-width:80rpx;display:inline-block">
                <view class="viewer_avatar">
                  <template is="image_keep_ratio_hor_fill_ver_center"
                    data="{{src:item.avatarUrl || (item.gender == 'F' ? globalData.fallbackAvatarUrlWoman: globalData.fallbackAvatarUrlMan)}}"
                    />
                  <!--
                  <image style="width:100%"
                    src="{{item.avatarUrl || (item.gender == 'F' ? globalData.fallbackAvatarUrlWoman: globalData.fallbackAvatarUrlMan)}}"
                    mode="widthFix"
                    />
                    -->
                </view>
                <view class="viewer_name"
                  >{{item.name}}</view>
              </view>
            </block>
          </scroll-view>
        </view>
        <view style="margin-top: 20rpx;"/>
        <view class="weui-flex" style="align-items:flex-end;vertical-align:bottom">
          <view style="font-size:80%;color:grey"
            wx:if="{{commentUserCount}}"
            >
            <text>{{commentList[0].name}}等{{commentUserCount}}人评论了此文</text>
            <text wx:if="{{replyBonus}}">，各获得{{replyBonus}}积分</text>
            <text>，共{{commentCount||0}}条</text>
          </view>
          <view class="weui-flex__item"/>
        </view>
        <view class="div-hor" style="width:100%;margin-top:20rpx;margin-left:0;margin-bottom:20rpx"/>
        <!-- 评论列表 -->
        <block wx:for="{{commentList}}" wx:key="id">
          <template is="comment-simple" data="{{globalData,...item,index,canComment}}"/>
        </block>
      </block>
    </block>
  </scroll-view>
  <view class="article-reply-panel-placeholder"
    style="height: {{page.bottomPanel.height}}rpx"/>
  <view class="article-reply-panel">
    <block wx:if="{{canComment}}">
      <!-- 引用回复 -->
      <view class="article-reply-panel-ref"
        wx:if="{{replying && commentType=='reply'}}">
        <text>回复{{commentList[commentIdx].displayOrder}}楼@{{commentList[commentIdx].nickName}}</text>
        <view class="article-reply-panel-ref-content"
          ><text>{{commentList[commentIdx].content}}</text></view>
      </view>
      <!-- 回复面板 -->
      <view class="weui-flex" style="align-items:flex-end">
        <block wx:if="{{textReply}}">
          <!-- 切换文字/语音 -->
          <image class="article-reply-panel-switch"
            bindtap="toggleReply"
            src="/icon/say.png"
            />
          <form
            bindsubmit="sendReply"
            bindreset="clearReply"
            >
            <view class="weui-flex"
              style="align-items:flex-end;width:630rpx"
              >
              <textarea class="article-reply-panel-input"
                name="input"
                focus="{{replying}}"
                value="{{inputValue}}"
                auto-height="{{autoHeight}}"
                style="height:{{inputHeight}}rpx;font-size:{{inputFont}}rpx;line-height:{{inputLine}}rpx;min-height:{{inputLine}}rpx"
                show-confirm-bar="{{false}}"
                placeholder="回复{{myReplies||readBonus==0?'':'（首条+'+replyBonus+'分）'}}"
                bindfocus="commentFocus"
                bindblur="commentBlur"
                bindinput="commentInput"
                bindlinechange="commentResize"
                bindconfirm="sendReply"
                disabled="{{commentPending}}"
                />
              <view class="weui-flex__item"/>
              <button class="weui-btn article-reply-panel-send"
                type="primary"
                formType="submit"
                disabled="{{commentPending}}"
                wx:if="{{reply}}"
                >发送</button>
            </view>
          </form>
        </block>
        <block wx:else>
          <image class="article-reply-panel-switch"
            bindtap="toggleReply"
            src="/icon/keyword.png"
            />
          <view class="article-reply-panel-record"
            bindlongpress="record"
            bindtouchend="endRecord"
            >
            <text>按住</text><text style="margin-left:.5em"/><text>说话</text>
          </view>
          <view class="weui-flex__item"/>
        </block>
      </view>
    </block>
    <view wx:else>
      <view class="article_meeting_panel_label grey">{{statusText}}</view>
    </view>
  </view>
</view>
<!--view wx:else>页面加载中……</view-->
