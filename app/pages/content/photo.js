// page template
var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
Page({
  data: {
    showList: true,
    titleText: '活动相册',
  },
  onLoad: function(params) {
    // 为实现使用返回键在页面内跳转的功能，一次压入两个相同页面
    let pages = getCurrentPages()
    if(pages.length < 2 || pages[pages.length-2].route != this.route) {
      this.isBackground = true
    } else {
      this.bgPage = pages[pages.length-2]
      util.setThis(this.bgPage, {navigateBack: true})
    }
    util.setThis(this, {globalData: app.globalData})
      .then(()=>params)
      .then(util.log.onLoad)
      .then(d=>{this.setData(d)})
      .then(()=>this.init())
      .then(()=>this.setData({loaded:true}))
      .then(()=>this.onShow())
  },
  onReady: function() {
    this.setData({ready: true}, this.onShow.bind(this))
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      if(this.isBackground) {
        if(this.data.navigateBack) {
          if(this.data.showList) {  // return from list mode: return
            wx.navigateBack()
          } else {                  // return from single mode: goto listmode
            this.setData({showList: true}, ()=>wx.navigateTo({url: '/'+this.route}))
          }
        } else {
          wx.navigateTo({url: '/'+this.route})
        }
      } else {
        audio.register()
      }
    }
  },
  init: function() {
    if(this.isBackground) {
      return api.get('/album?id='+this.data.id)
        .then(util.log.getAlbum)
        .then(res=>{
          if(res.success) {
            return util.setThis(this, res.data)
              .then(()=>this.update())
          }
        })
        .then(()=>{
          if(this.data.debug) {
            return util.setThis(this, {
              showList: this.data.showList != '0',
              currentId: parseInt(this.data.currentId),
            })
          }
        })
    } else {
      return util.setThis(this, this.bgPage.data)
    }
  },
  onImageLoad: function(event) {
    util.log.onImageLoad(event)
    this.setData({['imageLoad['+event.target.id+']']: true})
  },
  onUnload: function() {
  },
  update: function() {
    return util.setData({imageLoad: new Array(this.data.photos.length).fill(false)})
  },
  prevImage: function(event) {
    if(this.data.currentId > 0) {
      this.setData({currentId: this.data.currentId-1})
    }
  },
  nextImage: function(event) {
    if(this.data.currentId < this.data.photos.length - 1) {
      this.setData({currentId: this.data.currentId+1})
    }
  },
  tapSingle: function(event) {
    util.log.tapSingle(event)
    this.setData({
      showList: false,
      currentId: parseInt(event.currentTarget.id),
      titleText: '照片详情',
    }, util.updateTitle)
    if(this.bgPage) {
      util.setThis(this.bgPage, {showList: false})
    }
  },
})
