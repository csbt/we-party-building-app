// page template
var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
const PREV_ROUTE = 'pages/content/test'
const ITEM_PER_ROW = 10
const ROW_HEIGHT = 74
Page({
  data: {
    hidden: true,
    ABC: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
      .then(api.log)
      .then(d=>{this.setData(d)})
      .then(this.init)
      .then(()=>this.setData({loaded:true}))
      .then(this.onShow)
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updatePage()
      audio.register()
    }
  },
  nav: function(event) {
    console.log(event)
    this.setData({currentQuestion: parseInt(event.target.id)})
  },
  resultNav: function(event) {
    console.log(event)
    this.setData({scrollTo: 'question'+event.target.id})
  },
  toggleHidden: function(event) {
    let hidden = !this.data.hidden
    let height
    if(hidden) {
      height = 0
    } else {
      // TODO
      height = (Math.floor((this.data.numQuestions-1) / ITEM_PER_ROW) + 1) * ROW_HEIGHT
    }
    this.setData({
      hidden,
      'page.topNav': {height},
    })
  },
  init: function() {
    let testPage = util.getPrevPage()
    if(!testPage || testPage.route != PREV_ROUTE) {
      api.error(`expecting page ${PREV_ROUTE} but got ${testPage}`)
    }
    let answers = wx.getStorageSync('test-'+testPage.data.id)
    if(answers) answers = JSON.parse(answers)
    util.setData({
      id: testPage.data.id,
      recordId: testPage.data.recordId,
      titleText: testPage.data.name,
      questions: testPage.data.questions,
      numQuestions: testPage.data.numQuestions,
      currentQuestion: testPage.data.currentQuestion || 0,
      answers: answers || testPage.data.answers,
      answersOri: testPage.data.answersOri,
      results: testPage.data.results,
      startAt: testPage.data.startAt,
      duration: testPage.data.duration,
      status: testPage.data.status,
    })
      .then(data=>{
        if(data.status == 'running') {
          api.post({
            url: '/echo', // latency test and time sync
            data: {postAt: Date.now()},
          })
            .then(res=>{
              let serverTime = new Date(res.data.receivedAt)
              let latUp = serverTime - new Date(res.data.request.postAt)
              let latDown = new Date() - serverTime
              console.log({latUp, latDown})
              let startAt = new Date(data.startAt)
              let endAt = new Date(startAt.getTime() + data.duration * 60 * 1000)
              let countDown = endAt - serverTime
              let answered = data.answers.map((answer,idx)=>{
                return answer !== null && (typeof(answer)=='number' || answer.indexOf(true)!=-1)
              })
              return util.setData(
                {
                  startAt, 
                  endAt, 
                  countDown, 
                  localError: latDown,
                  'page.topPanel': {height: 80},
                  'page.bottomNav': {height: 160},
                  answered,
                })
                .then(()=>this.updateCD(0))
            })
        } else {
          let height = (Math.floor((this.data.numQuestions-1) / ITEM_PER_ROW) + 1) * ROW_HEIGHT
          console.log({height})
          this.setData({'page.bottomNav': {height}})
        }
      })
  },
  updateCD: function(expectedInterval) {
    if(util.getPage() != this) {
      console.info('updateCD: STOP on page hidden')
      return
    }
    let countDown = this.data.endAt - new Date() + this.data.localError
    const DECENT_ERROR = 1000
    const CD_INTERVAL = 1000
    return Promise.resolve(this.data.countDown - countDown - expectedInterval)
      .then(error=>{
        console.log('updateCD:', error)
        if(error > DECENT_ERROR || error < -DECENT_ERROR) {
          console.error('updateCD: ERROR too big, adjust clock')
          return api.post(
            {
              url: '/echo', 
              data: {
                source: 'question.updateCD.error',
                error,
                recordId: this.data.recordId,
                log: true,
              }
            })
            .then(res=>{
              let serverTime = new Date(res.data.receivedAt)
              let localError = new Date() - serverTime
              countDown = this.data.endAt - serverTime
              this.setData({localError})
            })
        }
      })
      .then(()=>{
        if(countDown > 0) {
          this.setData({
            countDown,
            countDownDisplay: util.msToClock(countDown),
          })
          setTimeout(()=>this.updateCD(CD_INTERVAL), CD_INTERVAL)
        } else {
          return this.doSubmit('超时自动交卷')
        }
      })
  },
  checkboxChange(event) {
    console.log(event)
    let answer = this.data.answers[parseInt(event.target.id)].map((_,idx)=>event.detail.value.indexOf(idx.toString())!=-1)
    util.setData({
      ['answers['+event.target.id+']']: answer,
      ['answered['+event.target.id+']']: answer.indexOf(true) != -1,
    })
    .then(data=>{
      wx.setStorage({
        key: 'test-'+data.id,
        data: JSON.stringify(data.answers)
      })
    })
  },
  radioChange(event) {
    console.log(event)
    util.setData({
      ['answers['+event.target.id+']']: parseInt(event.detail.value),  // TODO: wx bug ? string was set
      ['answered['+event.target.id+']']: true,
    })
    .then(data=>{
      wx.setStorage({
        key: 'test-'+data.id,
        data: JSON.stringify(data.answers)
      })
    })
  },
  back: function() {
    wx.navigateBack()
  },
  prev: function() {
    if(this.data.currentQuestion) {
      this.setData({currentQuestion: this.data.currentQuestion - 1})
    }
  },
  next: function() {
    if(this.data.currentQuestion < this.data.numQuestions - 1) {
      this.setData({currentQuestion: this.data.currentQuestion + 1})
    }
  },
  submit: function() {
    let left = []
    this.data.answers.forEach((answer,idx)=>{
      if(answer === null || (typeof(answer)=='object' && answer.indexOf(true)==-1)) {
        left.push(idx+1)
      }
    })
    if(left.length) {
      util.showModal({
        title: '考试未完成', 
        content: `有${left.length}道题目（题${left[0]}${left.length>1?'，...':''}）尚未做出选择`,
      })
    } else {
      wx.showModal({
        title: '确认交卷', 
        //content: left ? `有${left}道题目尚未做出选择`:'全部题目已完成',
        success: res=>{
          if(res.confirm) {
            this.doSubmit()
          }
        },
      })
    }
  },
  doSubmit: function(title) {
    this.setData({loaded: false})
    wx.showLoading({title: title||'交卷中'})
    return api.postExam(this.data)
      .then(api.log)
      .then(res=>{
        util.getPrevPage().setData(res)
        wx.hideLoading()
        let content = `得分：${Math.floor(res.score*100/res.numQuestions+.5)}分（${res.score}/${res.numQuestions}）`
        if(res.totalUsers) {
          let percent = Math.floor((res.totalUsers - res.betterUsers - 1) * 100 / (res.totalUsers - 1) + 0.5)
          content += `\r\n击败了${percent}%的党员`
        }
        util.showModal(
          {
            title: res.status == 'passed' ? '考试通过' : '考试未通过',
            content,
          })
          .then(()=>wx.navigateBack())
      })
  },
})
