<import src="/utils/templates.wxml"/>
<import src="/utils/audio.wxml"/>
<view wx:if="{{loaded}}">
  <template is="audio-new" data="{{globalData,...audio,height:page.audio.height}}"/>
  <view>
    <!-- 进行中的考试 -->
    <block wx:if="{{status=='running'}}">
      <!-- 计时器 + 可展开的题目列表 -->
      <view style="height:{{page.topPanel.height}}rpx;"/>
      <view class="weui-flex question_top_panel"
        style="height:{{page.topPanel.height}}rpx;line-height:{{page.topPanel.height}}rpx;"
        >
        <view class="question_top_panel_timer"
          >当前第{{currentQuestion+1}}/{{numQuestions}}题</view>
        <view style="margin-left: .5em"/>
        <view class="question_top_panel_timer"
          >剩余时间：{{countDownDisplay||countDown}}</view>
        <view class="weui-flex__item"/>
        <view bindtap="toggleHidden"
          >{{hidden?'⏬':'⏫'}}</view>
      </view>
      <view class="weui-flex question_top_panel_nav"
        wx:if="{{!hidden}}"
        style="height:{{page.topNav.height}}rpx"
        >
        <block wx:for="{{questions}}" wx:key="id">
          <view class="question_panel_nav_item {{answered[index]?'question_panel_nav_item_answered':''}}"
            style="{{index==currentQuestion?'color:white':''}}"
            bindtap="nav"
            id="{{index}}"
            >{{item.pos}}</view>
        </block>
      </view>

      <!-- 题目 -->
      <scroll-view class="question_body"
        scroll-y="{{true}}"
        >
        <view style="height:{{page.topNav.height}}rpx"/>
        <block wx:for="{{questions}}" wx:key="id">
          <block wx:if="{{index==currentQuestion}}">
            <view class="question_text"
              >第{{item.pos}}题（{{item.type=='SINGLE'?'单选':'多选'}}）：{{item.text}}</view>
            <!-- 单选 -->
            <block wx:if="{{item.type=='SINGLE'}}">
              <radio-group class="radio-group" bindchange="radioChange" id="{{index}}">
                <block class="radio" wx:for="{{item.options}}" wx:key="*this"
                  wx:for-item="option" wx:for-index="optionIdx">
                  <label class="weui-flex question_option {{answers[index]==optionIdx?'question_option_selected':''}}"
                    >
                    <radio class="question_option_control" value="{{optionIdx}}" checked="{{answers[index]==optionIdx}}"/>
                    <view class="question_option_text">{{ABC[optionIdx]}}. {{option}}</view>
                  </label>
                </block>
              </radio-group>
            </block>
            <!-- 多选 -->
            <block wx:else>
              <checkbox-group bindchange="checkboxChange" id="{{index}}">
                <block wx:for="{{item.options}}" wx:key="*this"
                  wx:for-item="option" wx:for-index="optionIdx">
                  <label class="weui-flex question_option {{answers[index][optionIdx]?'question_option_selected':''}}"
                    >
                    <checkbox class="question_option_control" value="{{optionIdx}}" checked="{{answers[index][optionIdx]}}"/>
                    <view class="question_option_text">{{ABC[optionIdx]}}. {{option}}</view>
                  </label>
                </block>
              </checkbox-group>
            </block>

          </block>
        </block>
      </scroll-view>
      <!-- 导航按键 -->
      <view style="height:{{page.bottomNav.height}}rpx"/>
      <view class="question_nav"
        style="height:{{page.bottomNav.height}}rpx"
        >
        <view class="div-hor" style="width:100%;margin:0"/>
        <view class="weui-flex question_nav_panel">
          <view/>
          <button class="weui-btn question_nav_btn" type="primary"
            bindtap="prev"
            wx:if="{{currentQuestion}}"
            >上一题</button>
          <button class="weui-btn question_nav_btn" type="default"
            bindtap="back"
            wx:else
            >回首页</button>
          <view style="margin-left:30rpx"/>
          <button class="weui-btn question_nav_btn" type="primary"
            bindtap="next"
            wx:if="{{currentQuestion < numQuestions-1}}"
            >下一题</button>
          <button class="weui-btn question_nav_btn" type="primary"
            bindtap="submit"
            style="background-color: orange"
            wx:else
            >提交</button>
        </view>
      </view>
    </block>
    <!-- 结束的考试 -->
    <block wx:else>
      <scroll-view scroll-y="true" scroll-into-view="{{scrollTo}}" scroll-with-animation="{{true}}"
        style="height:{{listHeight}}px"
        >
        <block wx:for="{{questions}}" wx:key="id">
          <view class="question_result_detail" id="question{{index}}">
            <view class="question_text"
              >第{{item.pos}}题（{{item.type=='SINGLE'?'单选':'多选'}}）：{{item.text}}</view>
            <block wx:if="{{item.type=='SINGLE'}}">
              <radio-group class="radio-group">
                <block class="radio" wx:for="{{item.options}}" wx:key="*this"
                  wx:for-item="option" wx:for-index="optionIdx">
                  <view class="weui-flex question_option_after">
                    <radio class="question_option_control" disabled="{{true}}" checked="{{answers[index]==optionIdx}}"/>
                    <view class="question_option_text">{{option}}</view>
                  </view>
                </block>
              </radio-group>
            </block>
            <!-- 多选 -->
            <block wx:else>
              <checkbox-group>
                <block wx:for="{{item.options}}" wx:key="*this"
                  wx:for-item="option" wx:for-index="optionIdx">
                  <view class="weui-flex question_option_after">
                    <checkbox class="question_option_control" disabled="{{true}}" checked="{{answers[index][optionIdx]}}"/>
                    <view class="question_option_text">{{option}}</view>
                  </view>
                </block>
              </checkbox-group>
            </block>
            <view class="div-hor"/>
            <view class="question_result_answer">
              <text>答案：</text>
              <text wx:if="{{results[index]=='F'}}" class="question_result_answer_false">{{answersOri[index]||'未选'}}</text>
              <text class="question_result_answer_true">{{item.answer}}</text>
            </view>
          </view>
        </block>
      </scroll-view>
      <view class="weui-flex question_bottom_nav_panel"
        style="height:{{page.bottomNav.height}}rpx;"
        >
        <block wx:for="{{questions}}" wx:key="id">
          <view class="question_panel_nav_item {{results[index]=='T'?'question_panel_nav_item_true':results[index]=='F'?'question_panel_nav_item_false':''}}"
            bindtap="resultNav"
            id="{{index}}"
            >{{item.pos}}</view>
        </block>
      </view>
    </block>
  </view>
</view>
