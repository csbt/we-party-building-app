// page template
var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
var socket = require('../../utils/socket')
const DEFAULT_BEAUTY = 6.3
const DEFAULT_WHITENESS = 3.0
Page({
  data: {
    pushing: false,
    //frontCamera: false,
    showHDTips: false, //显示清晰度弹窗
    mode: "SD",
    muted: false,
    orientation: "vertical",
    beauty: DEFAULT_BEAUTY,
    whiteness: DEFAULT_WHITENESS,
    hide: false,
    //titleColor: {backgroundColor: '#ef4f01', frontColor: '#ffffff'},
    titleText: '会议直播测试',
    recordingDotColor: 'red',
    playerHeight: '100%',
    keyboardHeight: '0',
    commentScroll: 0,
  },
  audioHandler: function(event) {
    console.log('audioHandler',event.currentTarget.dataset.method,event.target.id)
    if(event.currentTarget.dataset.method == 'switchSpeaker') {
      audio.switchSpeaker(parseInt(event.detail.value))
    } else {
      audio[event.currentTarget.dataset.method](event.currentTarget.id)
    }
  },
  onLoad: function(params) {
    util.setThis(this, {globalData: app.globalData, ...params})
      .then(util.log.onLoad)
      .then(()=>this.init())
      .then(()=>util.setThis(this, {loaded:true}))
      .then(()=>this.onReady())
  },
  onStateChange: function(event) {
    util.log.stateChange(event.detail.message)
  },
  onReady: function() {
    if(!this.data.ready && this.data.loaded) {
      util.log.onReady()
//      this.pusher = wx.createLivePusherContext('pusher')
      //const COVER_VIEW_SCROLL_TOP_INTRODUCED = '2.1.0'
      //this.setData({compMode: !util.versionGt(COVER_VIEW_SCROLL_TOP_INTRODUCED)})
      util.setThis(this, {ready: true})
        .then(()=>this.onShow())
    }
  },
  startPush: function() {
    util.log.startPush()
    wx.setKeepScreenOn({keepScreenOn: true, success: util.log.keepScreenOn})
    if(!this.pusher) {
      this.pusher = wx.createLivePusherContext('pusher')
    }
    socket.stream.pushStart({id: this.data.id})
      .then(util.log.pushStart)
      .then(res=>{
        return util.setThis(this, {
          streamer: app.globalData.userInfo.appUser.id,
          pushUrl: res.pushUrl
        })
      })
      .then(()=>{
        this.pusher.start()
        this.setData({pushing: true})
        setTimeout(this.scroll.bind(this), 5000)
      })
  },
  startPlay: function() {
    util.log.startPlay()
    if(!this.player) {
      this.player = wx.createLivePlayerContext('player')
    }
    this.player.requestFullScreen({
      complete: ()=>{
        util.log.fullScreenComplete()
        this.player.play()
      },
    })
    //this.player.play()
    setTimeout(this.scroll.bind(this), 5000)
  },
  stopPlay: function() {
    if(this.player) this.player.stop()
  },
  togglePlay: function() {
    console.log(this.pusher)
    this.pusher.start({
      success: util.log.startSuccess,
      fail: util.log.startFail,
    })
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      audio.register()
      if(this.data.streamer) {  // 有人直播
        if(this.data.streamer == app.globalData.userInfo.appUser.id) {  // 是主播
          this.startPush()
        } else { // 是观众
          this.startPlay()
        }
      } else {  // 没有源
      }
      //this.setData({
      //  commentsScrollAnimation: wx.createAnimation({
      //    duration: 3000,
      //    timingFunction: "ease-in-out",
      //    delay: 5000,
      //  }).bottom(0).step().export(),
      //})
    }
  },
  echo: function(event) {
    util.log.echo(event)
  },
  onPusherStateChange: function(event) {
    util.log.pusher(event.detail.message)
  },
  onPushClick: function(event) {
    wx.setKeepScreenOn({keepScreenOn: false, success: util.log.keepScreenOff})
    this.pusher.stop()
    socket.stream.pushStop({id: this.data.id})
    this.setData({
      pushing: false,
      streamer: null,
    })
    wx.navigateBack()
  },
  onSwitchCameraClick: function(event) {
    //this.setData({frontCamera: !this.data.frontCamera})
    this.pusher.switchCamera({
      success: util.log.SCsuccess,
      fail: util.log.SCfail,
    })
  },
  onBeautyClick: function(event) {
    this.setData({
      beauty: this.data.beauty ? 0 : DEFAULT_BEAUTY,
      whiteness: this.data.whiteness ? 0 : DEFAULT_WHITENESS,
    })
  },
  ignore: ()=>{},
  scroll: function() {
    let tag = this.data.pushing ? '#pusher_comments' : '#player_comments'
    wx.createSelectorQuery().select(tag + '_container').boundingClientRect(container=>{
      wx.createSelectorQuery().select(tag).boundingClientRect(item=>{
        util.log.container(container)
        util.log.item(item)
        let commentScroll = item.height - container.height
        util.log.commentScroll(commentScroll)
        if(commentScroll > 0) {
          this.setData({commentScroll})
        }
      }).exec()
    }).exec()
  },
  temp: function() {
    util.log.temp()
    this.setData({['commentList['+this.data.commentList.length+']']: {name: this.data.commentList.length, content: new Date()}})
    wx.createSelectorQuery().select('#pusher_comments_container').boundingClientRect(container=>{
      wx.createSelectorQuery().select('#pusher_comments').boundingClientRect(item=>{
        let commentScroll = item.height - container.height
        if(commentScroll > 0) {
          this.setData({commentScroll})
        }
      }).exec()
    }).exec()
  },
  onSwitchMode: function() {
    this.setData({
      showHDTips: !this.data.showHDTips
    })
    //this.temp()
  },
  onModeClick: function (event) {
    let mode = event.target.dataset.mode || event.currentTarget.dataset.mode
    util.log.onModeClick(mode)
    this.setData({
      mode,
      showHDTips: false
    })
  },
  onHeartBeat: function(count) {
    if(this.data.pushing) {
      this.setData({recordingDotColor: this.data.recordingDotColor == 'red' ? 'white' : 'red'})
      //api.post(
      //  {
      //    url: '/document/'+this.data.id+'/stream',
      //    data: {start: true},
      //  })
      ////    .then(util.log.heartBeat)
      //  .then(res=>{
      //    if(res.success) {
      //      return res.data
      //    } else {
      //      return util.log.touchServerError(res)
      //    }
      //  })
    }
    if(this.data.showInput && (count%2)==0) {
      this.setData({showCursor: !this.data.showCursor})
    }
  },
  init: function() {
    if(this.data.type == 'meeting') {
      socket.stream.subscribe({id: this.data.id})
      let prevPage = util.getPrevPage()
      if(prevPage && prevPage.route == 'pages/content/meeting') {
        this.prevPage = prevPage
        util.setThis(this,
          {
            streamer: prevPage.data.streamer,
            streamerName: prevPage.data.streamerName,
            commentList: prevPage.data.commentList,
            titleText: prevPage.data.titleText,
            //playUrl: "rtmp://25840.liveplay.myqcloud.com/live/25840_7282cf74a1",
          })
          .then(()=>socket.stream.join({id: this.data.id}))
          .then(util.log.joinEcho)
          .then(res=>{
            if(res.pusher && res.pusher in res.clients) {
              return util.setThis(this, {
                streamer: res.clients[res.pusher].id,
                streamerName: res.clients[res.pusher].name,
                playUrl: res.playUrl,
                pushUrl: res.pushUrl,
              })
            }
          })
      } else {
        this.setData({
          streamer: 1000,
          playUrl: 'rtmp://live.hkstv.hk.lxdns.com/live/hks'
        })
        api.requireCom(this.data.id)
          .then(d=>util.setThis(this,{commentList: d}))
          .then(()=>socket.stream.join({id: this.data.id}))
      }
    }
    //this.ctx = wx.createLivePlayerContext(this.data.id)
  },
  onUnload: function() {
    socket.stream.leave({id: this.data.id})
    if(this.data.pushing) this.onPushClick()
  },
  typing: function(event) {
    this.setData({inputValue: event.detail.value})
    util.log.typing(event.detail.value)
  },
  blur: function(event) {
    if(this.data.showInput) {
      util.setThis(this, 
        {
          showInput: false,
          keyboardHeight: '0',
        })
        .then(this.scroll.bind(this))
    }
  },
  onNotify: function(data) {
    util.log.onNotify(data)
    switch(data.event) {
      case 'post':  // someone posted a message
        if(this.prevPage) {
          this.prevPage.setData({['commentList['+this.prevPage.data.commentList.length+']']: data.data})
        }
        util.setThis(this, {['commentList['+this.data.commentList.length+']']: data.data})
          .then(()=>this.scroll())
        break
      case 'join':
        util.setThis(this, {['commentList['+this.data.commentList.length+']']: {hint: data.data + '加入了直播间'}})
        break
      case 'leave':
        util.setThis(this, {['commentList['+this.data.commentList.length+']']: {hint: data.data + '离开了直播间'}})
        break
      case 'pushStart':
        util.setThis(this, 
          {
            streamer: data.data.id,
            streamerName: data.data.name,
            playUrl: data.data.playUrl,
            ['commentList['+this.data.commentList.length+']']: {hint: data.data.name + '开始了直播'},
          })
          .then(()=>this.startPlay())
        break
      case 'pushStop':
        util.setThis(this, 
          {
            streamer: null,
            streamerName: null,
            ['commentList['+this.data.commentList.length+']']: {hint: data.data.name + '停止了直播'},
          })
          .then(()=>this.stopPlay())
        break
      default:
        util.log.unknownEvent(data.event)
    }
  },
  writeComment: function(event) {
    util.setThis(this,
      {
        showInput: true,
      })
      .then(util.getSystemInfo)
      .then(util.log.getSystemInfo)
      .then(res=>{
        //this.setData({keyboardHeight: res.windowHeight/2 + 'px'})
        this.setData({keyboardHeight: res.windowHeight*0.55 + 'px'})
      })
  },
  submitComment: function() {
    if(this.data.inputValue && this.data.inputValue.length) {
      // TODO: submit, clear, refresh list
      let content = this.data.inputValue
      this.setData({inputValue:''})
      socket.stream.post({
        id: this.data.id,
        content,
      })
    }
    this.blur()
  },
  resetInput: function(event) {
    this.setData({
      showInput: false,
      playerHeight: '100%'
    })
  },
  printHeight: function() {
    util.log.printHeight()
    wx.createSelectorQuery().select('.player_page').boundingClientRect(util.log.player_page).exec()
    wx.createSelectorQuery().select('.stream_player').boundingClientRect(util.log.stream_player).exec()
  },
  playerFullScreenChange: function(event) {
    util.log.playerFullScreenChange(event)
    this.setData({fullScreen: event.detail.fullScreen})
  },
  toggleFullScreen: function() {
    wx.navigateBack()
    return
    if(this.data.fullScreen) {
      this.player.exitFullScreen()
    } else {
      this.player.requestFullScreen()
    }
  },
})
