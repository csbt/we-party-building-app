// page template
var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
// 采用双页面来覆盖返回键（上一题）
Page({
  data: {
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
      .then(api.log)
      .then(d=>{this.setData(d)})
      .then(this.init)
      .then(()=>this.setData({loaded:true}))
      .then(this.onShow)
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      audio.register()
    }
  },
  init: function() {
    return api.getExam(this.data.id)
      .then(api.log)
      .then(d=>{this.setData(d)})
      .then(()=>{this.setData({titleText: this.data.name})})
  },
  checkResults: function(event) {
    wx.navigateTo({url:'/pages/content/question'})
  },
  testStart: function(event) {
    if(this.data.status == 'open') {
      api.startExam(this.data.id)
        .then(api.log)
        .then(d=>{this.setData(d)})
        .then(()=>{wx.removeStorageSync('test-'+this.data.id)})
        .then(()=>{wx.navigateTo({url:'/pages/content/question'})})
    } else {
      wx.navigateTo({url:'/pages/content/question'})
    }
  },
  doStart: function() {
    this.setData({
      testing: true,
      idx: this.data.idx || 1,
    })
  },
})
