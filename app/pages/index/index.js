//index.js
var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
var {debug,customer} = require('../../config')
Page({
  data: {
    titleText: customer.titleText,
    titleColor: {frontColor:'#000000', backgroundColor:'#f6f6f6'},
    testList: [
      [{text: "本年个人积分："}, {text: 100, style: "color:orange"}, {text: "，排名："}, {text: 51, style: "color:orange"}],
      [{text: "本月个人积分："}, {text: 200, style: "color:orange"}, {text: "，排名："}, {text: 52, style: "color:orange"}],
      [{text: "本年支部积分："}, {text: 300, style: "color:orange"}, {text: "，排名："}, {text: 54, style: "color:orange"}],
    ],
    takeSession: false,
    requestResult: '',
    news: [],
    showAnimation: !debug && customer.animation != "none",
  },
  onLoad: function(params) {
    Promise.resolve(params)
      .then(util.log.indexParams)
      .then(d=>{this.setData(d)})
      .then(()=>{this.setData({globalData: app.globalData})})
      .then(()=>this.onEndLogin())
      .then(()=>this.onShow())
  },
  onReady: function() {
    if(!debug) {  // animation
      util.setThis(this,
        {  // set title color and hide text
          titleText: '',
          titleColor: {frontColor:'#ffffff', backgroundColor:app.globalData.customer.animationTitleColor || app.globalData.customer.animationColor},
        })
        .then(()=>util.updateTitle(this))
        .then(()=>this.aniStart())
    } else {
      this.aniEnd()
    }
    this.setData({ready: true}, ()=>this.onShow())
  },
  onShow: function() {
    util.log.onShow()
    if(this.data.ready && !this.data.showAnimation) {
      this.doSkipAni()
      if(this.data.loaded) {
        audio.register()
        if(this.data.scene) { // 扫码进入，会议/活动签到后跳转对应页面
          let scene = this.data.scene
          let [type, id] = scene.split('-')
          util.setThis(this, {scene: null})
            .then(()=>api.post({url: '/code', data: {path: scene}}))
            .then(util.log.code)
            .then(res=>util.showModal({content: res.message})
              .then(()=>{
                if(res.success) {
                  wx.navigateTo({url: app.globalData.urlMap[type]+'?id='+id})
                }
              })
            )
        } else if(app.globalData.userInfo.appUser.memorialHint) {
          wx.navigateTo({url: '/pages/user/bless?hint=true&id='+app.globalData.userInfo.appUser.id})
        }
      } else if(app.globalData.logFinish) {
        if(app.globalData.logged) {
          this.onEndLogin()
        } else {  // 未绑定用户，跳转绑定页面
          wx.navigateTo({url: '/pages/user/verify'})
        }
      }
    }
  },
  switchTab: function(event) {
    console.log(event)
    wx.switchTab({url: event.currentTarget.dataset.url})
  },
  onShareAppMessage: function () {
    return {
      title: '最具智慧的微信党建小程序',
      path: '/pages/index/index'
    }
  },
  aniStart: function() {
    if(customer.animation == 'none') {
      this.aniEnd()
      return
    }
    setTimeout(this.aniEnd.bind(this), 7000)
    if(customer.animation == 'oulu') {
      if(this.data.showAnimation) {
        let animation = wx.createAnimation({
          transformOrigin: "50% 50%",
          duration: 3000,
          timingFunction: "ease",
          delay: 0
        })
        this.setData({
          anim1: animation.opacity(1).step().export(),
        })
        setTimeout(this.ani2.bind(this), 2000)
        setTimeout(this.ani3.bind(this), 4000)
      }
    }
  },
  ani2: function() {
    if(this.data.showAnimation) {
      let animation = wx.createAnimation({
        transformOrigin: "50% 50%",
        duration: 3000,
        timingFunction: "ease",
        delay: 0
      })
      this.setData({
        anim2: animation.opacity(1).scale(2).step().export(),
      })
    }
  },
  ani3: function() {
    if(this.data.showAnimation) {
      let animation = wx.createAnimation({
        transformOrigin: "50% 50%",
        duration: 3000,
        timingFunction: "ease",
        delay: 0
      })
      this.setData({
        anim3: animation.opacity(1).step().export(),
      })
    }
  },
  doSkipAni: function() {
    return util.setThis(this, 
      {
        titleText: customer.titleText,
        titleColor: {frontColor:'#000000', backgroundColor:'#f6f6f6'},
        showAnimation: false,
      })
      .then(()=>util.updateTitle(this))
      .then(()=>wx.showTabBar())
  },
  skipAni: function() {
    this.aniEnd()
  },
  aniEnd: function() {
    util.log.aniEnd()
    if(util.getPage() == this) {  // 还在主页
      if(this.data.ready && app.globalData.logFinish) {
        util.setThis(this,{showAnimation: false})
          .then(()=>this.onShow())
      } else {
        setTimeout(this.aniEnd.bind(this), 500)
      }
    }
  },
  onEndLogin: function() {
    if(!this.data.loaded 
      && app.globalData.logFinish 
      && app.globalData.logged) {
      return this.init()
        .then(()=>{util.setThis(this,{loaded: true})})
        .then(()=>this.onShow())
    }
  },
  onReachBottom: function(events) {
    console.log('onReachBottom')
    if(!this.data.noMoreNews) {
      this.getNews()
    }
  },
  getNews: function() {
    let options = {}
    if(this.data.news.length) {
      options.excludes = this.data.news.map(news=>news.docId)
    }
    this.setData({loadPending: true})
    return api.post(
      {
        url: '/news',
        data: options,
      })
      .then(api.log)
      .then(res=>{
        this.setData({loadPending: false})
        if(this.data.news.length && res.data.length < 10) {
          this.setData({noMoreNews: true})
        }
        if(res.success) {
          res.data.forEach(news=>{
            this.setData({
              ['news['+this.data.news.length+']']: {
                ...news,
                displayDate: util.getCnDate(new Date(news.modifiedAt)),
              }
            })
          })
        }
      })
  },
  init: function(options) {
    return util.setThis(this, {globalData: app.globalData})
      .then(()=>api.requirePage('/index'))
      .then(util.log.homePage)
      .then(d=>this.setData(d))
      .then(()=>api.get('/score'))
      .then(util.log.getScore)
      .then(res=>{
        this.setData({
          'testList[0][1].text': res.data.yScore,
          'testList[0][3].text': res.data.yRank,
          'testList[1][1].text': res.data.score,
          'testList[1][3].text': res.data.rank,
        })
      })
      .then(()=>api.get('/org'))
      .then(res=>{
        this.setData({
          'testList[2][1].text': res.data.branch.score || 0,
          'testList[2][3].text': res.data.branch.rank || '-',
        })
      })
      .then(this.getNews)
      .then(()=>{
        if(debug && debug.url) {
          wx.navigateTo({url: debug.url})
        }
      })
  },
})
