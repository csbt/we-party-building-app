// page template
var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
const SEARCH_DELAY = 2000
Page({
  data: {
  },
  onLoad: function(params) {
    util.setThis(this, {globalData: app.globalData})
      .then(()=>params)
      .then(util.log.onLoad)
      .then(d=>{this.setData(d)})
      .then(()=>this.init())
      .then(()=>this.setData({loaded:true}))
      .then(()=>this.onShow())
  },
  onReady: function() {
    util.setThis(this, {ready: true})
      .then(()=>this.onShow())
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      audio.register()
    }
  },
  update: function() {
    for(const idx in this.data.nodes) {
      this.setData({['nodes['+idx+'].displayTime']: util.getCnDate(this.data.nodes[idx].createTime)})
    }
  },
  init: function() {
    return api.get('/album')
      .then(util.log.getAlbum)
      .then(res=>{
        if(res.success) {
          this.setData({nodes: res.data}, this.update.bind(this))
        }
      })
  },
  checkInput: function() {
    if(!this.data.search.pending 
      && this.data.search.inputVal && this.data.search.inputVal != this.data.search.lastSearch
      && this.data.search.inputChangeAt && this.data.search.inputChangeAt < Date.now()-SEARCH_DELAY 
    ) {
      this.doSearch()
    }
  },
  doSearch: function() {
    let val = this.data.search.inputVal
    if(val != this.data.search.lastSearch) {
      this.setData({
        'search.pending': true,
        'request.pattern': val,
      })
      this.doRequest(res=>{this.setData(res)})
        .then(()=>{
          this.setData({
            'search.lastSearch': val,
            'search.inputChangeAt': Date.now(),
            'search.pending': false
          })
        })
    }
  },
  showInput: function () {
    this.setData({
      'search.inputShowed': true
    });
  },
  hideInput: function () {
    this.setData({
      'search.inputVal': "",
      'search.inputShowed': false
    });
    this.doSearch()
  },
  clearInput: function () {
    this.setData({
      'search.inputVal': ""
    });
    this.doSearch()
  },
  inputTyping: function (e) {
    console.log(e.detail.value)
    this.setData({
      'search.inputVal': e.detail.value,
      'search.inputChangeAt': Date.now()
    })
    setTimeout(this.checkInput, SEARCH_DELAY);
  },
  doRequest: function(callback) {
    this.setData({loadPending: true})
    let query = this.data.request
    if(this.data.nodes && this.data.nodes.length) {
      query.excludes = this.data.nodes.map(node=>node.id)
    }
    return Promise.resolve(query)
      .then(util.log.doRequest)
      .then(res=>api.getWithData({url:'/album', data: res}))
      .then(util.log.queryResult)
      .then(res=>{
        if(res.success) {
          this.setData({nodes: res.data}, this.update.bind(this))
        }
      })
        
   //   .then(api.listColumnWithQuery)
   //   .then(api.log)
   //   .then(res=>{
   //     this.setData({'request.len': res.nodes && res.nodes.length || 0})
   //     if(res.nodes) {
   //       res.nodes.forEach(item=>{
   //         item.displayTime = util.getCnDate(new Date(item.createdAt))
   //       })
   //     }
   //     return res
   //   })
   //   .then(res=>{
   //     if(callback && typeof(callback) == 'function') {
   //       return Promise.resolve(callback(res))
   //         .then(()=>this.setData({loadPending: false}))
   //         .then(()=>res)
   //     } else {
   //       return res
   //     }
   //   })
   //   .catch(api.error)
   //   .catch(e=>{this.setData({'request.len': 0})})
  },
})

