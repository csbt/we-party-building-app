// page template
var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
Page({
  data: {
    titleText: '在线测试',
    tab: 0,
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
      .then(api.log)
      .then(d=>{this.setData(d)})
      .then(this.init)
      .then(()=>this.setData({loaded:true}))
      .then(this.onShow)
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      audio.register()
    }
  },
  init: function() {
    return api.listExams()
      .then(api.log)
      .then(res=>this.setData({exams: res}))
  },
  switchTab: function(event) {
    console.log(event)
    this.setData({tab: parseInt(event.target.id)})
  },
  tapItem: function(event) {
    console.log(event)
    console.log({url: '/pages/content/test?id='+event.currentTarget.id||event.target.id})
    wx.navigateTo({url: '/pages/content/test?id='+event.currentTarget.id||event.target.id})
  }
})
