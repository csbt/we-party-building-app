var app = getApp()
var util = require('../../utils/util')
var api = require('../../utils/webApi')
var audio = require('../../utils/audio')
var DOW = '日一二三四五六'
const SEARCH_DELAY = 2000
Page({
  data: {
    search: {
      inputShowed: false,
      inputVal: "",
      inputChangeAt: 0,
      lastSearch: "",
      pending: false,
    },
    tabs: [],
    activeIndex: 0,
    sliderOffset: 0,
    sliderWidth: 25,
    labelOffset: 0,
    swiperIdx: 0,
    request: {
      tab: 0,
      pattern: '',
      limit: 10,
      len: 10,
    },
    banner: {
      nodes: [
        {id:1, imageUrl: '/智慧党建.jpg', title: '智慧党建'},
        {id:2, imageUrl: '/banner1.jpg',  title: 'banner1'},
        {id:3, imageUrl: '/banner2.jpg',  title: 'banner2'},
      ],
    },
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
      .then(api.log)
      .then(d=>{this.setData(d)})
      .then(this.init)
      .then(()=>this.setData({loaded:true}))
      .then(wx.hideLoading)
      .then(this.onShow)
  },
  onReady: function() {
    if(!this.data.loaded) {
      wx.showLoading({title:'加载中'})
    }
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      audio.register()
    }
  },
  init: function() {
    this.setData({
      request: {
        id: this.data.id,
        tab: this.data.tab||0,
        pattern: '',
        limit: 10,
        len: 10,
      },
    })
    return this.doRequest(res=>{this.setData(res)})
      .then(()=>this.setData({titleText: this.data.id == 4 && app.globalData.customer.profile=='xingsheng' ? '支部会议' : this.data.name}))
      .then(()=>this.setData({allTabs: ['全部', ...this.data.tabs]}))
      .then(()=>this.tabInit())
  },
  bannerChanged: function(event) {
    this.setData({swiperIdx: event.detail.current})
  },
  checkInput: function() {
    if(!this.data.search.pending 
      && this.data.search.inputVal && this.data.search.inputVal != this.data.search.lastSearch
      && this.data.search.inputChangeAt && this.data.search.inputChangeAt < Date.now()-SEARCH_DELAY 
    ) {
      this.doSearch()
    }
  },
  doSearch: function() {
    let val = this.data.search.inputVal
    if(val != this.data.search.lastSearch) {
      this.setData({
        'search.pending': true,
        'request.pattern': val,
      })
      this.doRequest(res=>{this.setData(res)})
        .then(()=>{
          this.setData({
            'search.lastSearch': val,
            'search.inputChangeAt': Date.now(),
            'search.pending': false
          })
        })
    }
  },
  showInput: function () {
    this.setData({
      'search.inputShowed': true
    });
  },
  hideInput: function () {
    this.setData({
      'search.inputVal': "",
      'search.inputShowed': false
    });
    this.doSearch()
  },
  clearInput: function () {
    this.setData({
      'search.inputVal': ""
    });
    this.doSearch()
  },
  inputTyping: function (e) {
    console.log(e.detail.value)
    this.setData({
      'search.inputVal': e.detail.value,
      'search.inputChangeAt': Date.now()
    })
    setTimeout(this.checkInput, SEARCH_DELAY);
  },
  doRequest: function(callback) {
    this.setData({loadPending: true})
    let query = this.data.request
    if(this.data.nodes && this.data.nodes.length) {
      query.excludes = this.data.nodes.map(node=>node.id)
    }
    return Promise.resolve(query)
      .then(api.log)
      .then(api.listColumnWithQuery)
      .then(api.log)
      .then(res=>{
        this.setData({'request.len': res.nodes && res.nodes.length || 0})
        if(res.nodes) {
          res.nodes.forEach(item=>{
            item.displayTime = util.getCnDate(new Date(item.createdAt))
          })
        }
        return res
      })
      .then(res=>{
        if(callback && typeof(callback) == 'function') {
          return Promise.resolve(callback(res))
            .then(()=>this.setData({loadPending: false}))
            .then(()=>res)
        } else {
          return res
        }
      })
      .catch(api.error)
      .catch(e=>{this.setData({'request.len': 0})})
  },
  onReachBottom: function() {
    console.log('onReachBottom')
    if(this.data.request.len >= this.data.request.limit) {
      this.doRequest(res=>{
        res.nodes = [...this.data.nodes, ...res.nodes]
        this.setData(res)
      })
    }
  },
  tabInit: function() {
    util.getSystemInfo()
      .then(res=> {
        let tab = this.data.request.tab
        let totalTabs = this.data.allTabs.length
        let visibleTabs = Math.min(4, totalTabs)
        let sliderWidth = res.windowWidth / visibleTabs
        let sliderOffset = tab * sliderWidth
        let labelOffset = 0
        if(tab && totalTabs > 4) {
          labelOffset = (tab-1)*res.windowWidth / 4
        }
        this.setData({
          sliderWidth,
          sliderOffset,
          labelOffset,
          labelWidth: Math.max(100, totalTabs * 25)
        })                                                                          
      })
  },
  genKey: function() {
    return `col-${this.data.id}-${this.data.request.tab}-${this.data.request.pattern||''}`
  },
  tabClick: function (e) {
    if(this.data.loadPending) return
    let destId = parseInt(e.currentTarget.id)
    if(this.data.request.tab === destId) return
    //api.save(this.genKey(), {nodes:this.data.nodes,request:this.data.request})
    if(this.data.allTabs.length > 4) {
      this.setData({
        labelOffset : (destId-1)*this.data.sliderWidth,
        sliderOffset : destId * this.data.sliderWidth,
        'request.tab': destId,
      })
    } else {
      this.setData({
        sliderOffset: e.currentTarget.offsetLeft,
        'request.tab': destId,
      })
    }
    //api.load(this.genKey())
    //  .then(res=>{this.setData(res)})
    //  .catch(()=>{
    //    this.doRequest(res=>{this.setData(res)})
    //  })
    this.setData({nodes:[]})
    this.doRequest(res=>{this.setData(res)})
  }
})
