var app = getApp()
var util = require('../../utils/util')
var api = require('../../utils/webApi')
var audio = require('../../utils/audio')
Page({
  data: {
    titleText: '会议',
    page: {
      backgroundMargin: {height: 350},
      bottomButtomMarginTop: {height: 40},
      bottomButtom: {height: 240},
      bottomButtomMarginBottom: {height: 40},
    },
    listHeight: 0,
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
      .then(api.log)
      .then(d=>{this.setData(d)})
      .then(this.init)
      //.then(()=>this.setData({loaded:true}))
      //.then(this.onShow)
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      audio.register()
      util.updatePage()
    // 通过微信扫码进入
      if(this.data.scene) {
        let scene = this.data.scene
        this.setData({scene:null})
        this.postCode({
          path: scene,
        })
      }
    // 其它页面调用扫码
      if(app.globalData.doScan) {
        app.globalData.doScan = false
        this.tapScan()
          .then(()=>{
            if(app.globalData.doScanThen && typeof(app.globalData.doScanThen) == 'function') {
              let f = app.globalData.doScanThen
              app.globalData.doScanThen = null
              return app.globalData.doScanThen()
            }
          })
      }
    }
    if(!this.data.loaded && app.globalData.logFinish) {
      this.onEndLogin()
    }
  },
  onEndLogin: function() {
    if(app.globalData.logged) {
      wx.showTabBar()
      this.init()
    } else {
      wx.navigateTo({url:'/pages/user/verify?navigateBack=1'})
    }
  },
  refresh: function() {
    return api.listMeeting()
      .then(api.log)
      .then(d=>{this.setData(d)})
  },
  init: function() {
    if(!app.globalData.logFinish) return
    if(!app.globalData.logged) return this.onEndLogin()
    this.setData({globalData: app.globalData})
    this.refresh()
      .then(()=>{this.setData({loaded: true})})
      .then(()=>{this.onShow()})
  },
  postCode: function(code) {
    return api.post(
      {
        url: '/code',
        data: code,
      })
      .then(api.log)
      .then(res=>{
        this.refresh()
          .then(()=>util.showModal({content: res.message}))
      })
      .catch(api.error)
      .catch(e=>util.showModal({content:'扫码失败：' + e,}))
  },
  tapScan: function() {
    return new Promise((resolve, reject)=>{
      wx.scanCode({
        onlyFromCamera: true,
        success: res=>resolve(res),
        fail: res=>reject(res),
      })
    })
      .then(util.log.tapScan)
      .then(res=>{
        return this.postCode({path: res.path.split('=')[1]})
      })
  },
})
