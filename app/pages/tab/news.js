var app = getApp()
var util = require('../../utils/util')
var audio = require('../../utils/audio')
var api = require('../../utils/webApi')
Page({
  data: {
    titleText: '党建动态',
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
    .then(api.log)
    .then(d=>{this.setData(d)})
    .then(()=>api.requirePage('/news'))
    .then(api.log)
    .then(d=>this.setData(d))
    .then(this.init)
    .then(api.log)
    .then(()=>this.setData({loaded:true}))
    .then(()=>this.onShow())
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.ready && this.data.loaded) {
      util.updateTitle()
      audio.register()
    }
  },
  init: function() {
    return Promise.all([
      //app.setTitleText('党建动态'),
      //app.setTitleColor({frontColor: '#ffffff',backgroundColor: '#123f60'})
    ])
    .then(()=>{
      this.setData({
      })
    })
  },
})
