var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
Page({
  data: {
    titleText: '我的党组织',
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
    .then(api.log)
    .then(d=>{this.setData(d)})
    .then(()=>api.requirePage('/org'))
    .then(api.log)
    .then(d=>this.setData(d))
    .then(this.init)
    .then(api.log)
    .then(()=>this.setData({loaded:true}))
    .then(()=>this.onShow())
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.ready && this.data.loaded) {
      audio.register()
      util.updatePage()
    }
  },
  onEndLogin: function() {
  },
  init: function() {
    return Promise.all([
      //app.setTitleText('我的党组织'),
      //app.setTitleColor({frontColor: '#ffffff',backgroundColor: '#123f60'})
    ])
    .then(()=>api.get('/org'))
    .then(util.log.getOrg)
    .then(res=>{
      if(res.success) {
        this.setData(res.data)
      }
    })
    .then(()=>{
      if(app.globalData.customer.profile == 'xingsheng') {
        return api.get('/score')
          .then(res=>{
            if(res.success) {
              this.setData(res.data)
            }
          })
      }
    })
  },
})
