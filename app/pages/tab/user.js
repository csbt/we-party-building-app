var app = getApp()
var api = require('../../utils/webApi')
var audio = require('../../utils/audio')
var util = require('../../utils/util')
Page({
  data: {
    titleText: '个人中心',
    tab: 0,
    entry: [
      {id: 1, imageUrl: '/我的/我的档案.jpg', labelText: '我的档案', disabled: false, url: '/pages/user/profile?id='},  // position fixed
      {id: 2, imageUrl: '/入党纪念.jpg', labelText: '政治生日', disabled: false, url: '/pages/user/bless?id='},
      //{id: 2, imageUrl: '/我的/我的收藏.jpg', labelText: '我的收藏<开发中>', disabled: true, url: '/pages/user/memorial'},
      {id: 3, imageUrl: '/我的/我的积分.jpg', labelText: '我的积分', disabled: false, url: '/pages/user/score'},
      //{id: 4, imageUrl: '/我的/我的评论.jpg', labelText: '我的评论<开发中>', disabled: true, url: '/pages/user/memorial'},
      {id: 5, imageUrl: '/我的/我的会议.jpg', labelText: '我的会议', disabled: false, url: '/pages/list/module?id=4'},  // TODO: 专门的页面
      {id: 6, imageUrl: '/我的/我的活动.jpg', labelText: '我的活动', disabled: false, url: '/pages/list/module?id=9'},  // TODO: 专门的页面
      {id: 7, imageUrl: '/我的/我的党费.jpg', labelText: '我的党费', disabled: false, url: '/pages/user/due'},
      {id: 8, imageUrl: '/我的/我的考试.jpg', labelText: '我的考试', disabled: false, url: '/pages/list/exam'},
    ],
  },
  tapMain: function(events) {
    console.log(events)
    let tab = parseInt(events.currentTarget.id)
    switch(tab) {
      case 2:
        api.get('/score?detail=true')
          .then(api.log)
          .then((res)=>{
            this.setData({nodes:res.map(d=>{
              return {
                ...d,
                displayTime: new Date(d.createdAt).toLocaleString().split(' ')[0]
              }})
            })
          })
          .then(()=>this.setData({tab}))
        break;
      default:
        console.error(tab)
    }
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      audio.register()
    }
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
    .then(api.log)
    .then(d=>{this.setData(d)})
    .then(this.init)
    .then(api.log)
    .then(()=>this.setData({loaded:true}))
    .then(()=>this.onShow())
  },
  init: function() {
    return Promise.all([
      //app.setTitleText('个人中心'), 
      //app.setTitleColor({frontColor: '#ffffff',backgroundColor: '#feb30c'})
    ])
    .then(()=>{
      this.setData({
        info: {
          name: app.globalData.userInfo.appUser.name,
          orgName: app.globalData.userInfo.appUser.orgName
        },
        'entry[0].url': this.data.entry[0].url + app.globalData.userInfo.appUser.id,
        'entry[1].url': this.data.entry[1].url + app.globalData.userInfo.appUser.id,
      })
    })
    .then(()=>api.get('/score'))
    .then(api.log)
    .then(res=>{
      this.setData(res.data)
    })
  },
})
