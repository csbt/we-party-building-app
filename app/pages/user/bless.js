// page template
var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
Page({
  data: {
    textReply: true,
    inputHeight: 60,
    inputLine: 60,
    inputFont: 40,
    inputValue: '',
    commentType: 'new',
    page: {
      bottomPanel: {height: 120}
    },
  },
  onLoad: function(params) {
    util.setThis(this, {globalData: app.globalData})
      .then(()=>params)
      .then(util.log.onLoad)
      .then(d=>{this.setData(d)})
      .then(()=>this.init())
      .then(()=>this.setData({loaded:true}))
      .then(()=>this.onShow())
  },
  onReady: function() {
    let ctx = wx.createInnerAudioContext()
    ctx.autoplay = true
    ctx.src = app.globalData.baseUrl + '/bgmusic.mp3'
    this.setData({ctx})
    util.setThis(this, {ready: true})
      .then(()=>this.onShow())
  },
  onUnload: function() {
    if(this.data.ctx) this.data.ctx.stop()
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      audio.register()
    }
  },
  init: function() {
    return api.get('/memorial?id='+this.data.id)
      .then(util.log.getBless)
      .then(res=>util.setThis(this,{...res.data}))
      .then(()=>{
        this.setData({titleText: this.data.name+'同志入党'+(this.data.years?this.data.years+'周年':'')+'纪念'}, util.updateTitle)
      })
      .then(()=>this.refreshComments())
      .then(()=>util.setThis(this, {isSelf: app.globalData.userInfo.appUser.id == this.data.id}))
      .then(()=>{
        if(this.data.hint) {
          return api.post({url: '/memorial?years='+this.data.years})
            .then(res=>{
              if(res.success) {
                app.globalData.userInfo.appUser.memorialHint = null
              }
            })
        }
      })
  },
  onMinuteChange: function() {
    this.refreshComments()
  },
  toggleReply: function() {
    this.setData({textReply: !this.data.textReply})
  },
  adjustInput: function(lineCount) {
    let inputHeight = Math.max(1, lineCount) * this.data.inputLine
    this.setData({
      inputHeight,
      'page.bottomPanel': {height: Math.max(inputHeight+20+20, 120)}
    }, util.adjustPageLayout)
  },
  commentResize: function(event) {
    console.log(event)
    this.adjustInput(event.detail.lineCount)
  },
  commentFocus: function(event) {
    this.setData({replying: true})
  },
  commentBlur: function(event) {
    this.setData({replying: false})
  },
  commentInput: function(event) {
    this.setData({reply:event.detail.value})
  },
  endRecord: function(event) {
    console.log('endRecord',event)
    wx.hideToast()
    wx.getRecorderManager().stop()
  },
  record: function(event) {
    wx.showToast({
      title: '',
      //icon: 'success',
      image: '/icon/record.png',
      duration: 60000,
    })
    console.log(event)
    const recorderManager = wx.getRecorderManager()
    recorderManager.onStart(() => {
      console.log('recorder start')
    })
    recorderManager.onStop((res) => {
      console.log('recorder stop', res)
      const { tempFilePath } = res
      console.log(tempFilePath)
      api.upload(tempFilePath)
        .then(api.log)
        .then(res=>{
          if(res.success) {
            this.setData({
              voice: res.data, 
              reply: '',
              inputValue: '',
            })
            this.sendReply()
          }
        })
        .catch(api.log)
        .then(this.clearComment.bind(this))
    })
    const options = {
      duration: 60000,
      sampleRate: 8000,
      numberOfChannels: 1,
      encodeBitRate: 32000,
      //format: 'aac',
      format: 'mp3',
    }
    recorderManager.start(options)
  },
  sendReply: function(event) {
    if(!this.data.commentPending) {
      let contentType
      if(this.data.voice) {
        contentType = 'audio'
      } else if(event && event.detail && event.detail.value && event.detail.value.input) {
        contentType = 'text'
      } else {
        return
      }
      this.setData({commentPending: true})
      let data = {
        type: this.data.commentType,
        content: contentType == 'text' ? event.detail.value.input : null,
        contentType,
        resId: contentType == 'audio' ? this.data.voice : null,
      }
      if(this.data.commentType !== 'new') {
        let id = this.data.commentList[this.data.commentIdx].id
        if(this.data.commentType === 'edit') {
          data.id = id
        } else if(this.data.commentType === 'reply') {
          data.refId = id
        }
      }
      api.post({
        url: '/document/'+this.data.docId+'/comment',
        data,
      })
      .then(api.log)
      .then(res=>{
        if(res.success) {
          if(this.data.commentType === 'edit') {
            this.refreshComment(this.data.commentIdx, {content:data.content})
          } else {
            this.setData({
              ['commentList['+this.data.commentList.length+']']: res.data,
              commentCount: this.data.commentCount + 1,
              commentUserCount: this.data.commentUserCount + (this.data.myReplies ? 0 : 1),
              myReplies: this.data.myReplies + 1,
            })
            this.refreshComment(this.data.commentList.length-1)
          }
        }
      })
      .catch(api.log) //ignore
      .then(this.clearComment.bind(this))
    }
  },
  clearComment: function() {
    this.setData({
      reply: '',
      inputValue: '',
      replying: false,
      commentPending: false,
      commentType: 'new',
      typing: false,
    })
    this.adjustInput(1)
  },
  tapCommentReply: function(event) {
    console.log(event)
    this.setData({
      commentType: 'reply',
      commentIdx: event.target.id,
      replying: true,
    })
  },
  tapCommentEdit: function(event) {
    this.setData({
      commentType: 'edit',
      commentIdx: event.target.id,
      reply: this.data.commentList[event.target.id].content,
      inputValue: this.data.commentList[event.target.id].content,
      replying: true,
    })
  },
  tapCommentTitleBar: function(event) {
    let id = event.currentTarget.id
    this.setData({['commentList['+id+'].showContent']: !this.data.commentList[parseInt(id)].showContent})
  },
  refreshComment: function(idx, data) {
    let update = {...this.data.commentList[idx], ...data}
    update.displayTime = util.showOldDate(update.createdAt)
    this.setData({['commentList['+idx+']']: update})
  },
  refreshComments: function() {
    for(const i in this.data.commentList) {
      this.refreshComment(i)
    }
  },
  playComment: function(event) {
    let id = parseInt(event.target.id)
    let comment = this.data.commentList[id]
    console.log(comment)
    let ctx = wx.createInnerAudioContext()
    ctx.autoplay = true
    ctx.onEnded(()=>{
      console.log('comment end')
      this.setData({['commentList['+id+'].playing']: false})
    })
    this.setData({['commentList['+id+'].playing']: true})
    ctx.onError(e=>{console.log('audio error:',e)})
    ctx.src = app.globalData.resRoot + comment.resId
  },
  hideHint: function(event) {
    this.setData({hint: null})
  },
})
