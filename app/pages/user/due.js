// page template
var app = getApp()
var api = require('../../utils/webApi')
var util = require('../../utils/util')
var audio = require('../../utils/audio')
Page({
  data: {
    titleText: '我的党费',
    records: [
    ],
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
      .then(api.log)
      .then(d=>{this.setData(d)})
      .then(this.init.bind(this))
      .then(()=>this.setData({loaded:true}))
      .then(this.onShow)
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      audio.register()
    }
  },
  init: function() {
    return api.get('/due')
      .then(res=>{
        if(!res.success) throw 'success:false' + res.data
        return res.data
      })
      .then(util.log.due)
      .then(records=>{
        let totalPay = 0
        records.forEach(record=>{
          record.priceText = util.getMoney(record.price)
          record.month = util.formatNumber(record.month)
          if(!record.paid) {
            record.selected = true
            totalPay += record.price
          }
        })
        this.setData({
          records,
          totalPay : util.getMoney(totalPay),
          titleText: this.data.globalData.userInfo.appUser.name + '同志的党费'
        })
      })
  },
  passed: function(event) {
    util.showModal({content: '线上支付暂未开通'})
  },
  pay: function(event) {
    util.showModal({content: '线上支付暂未开通'})
  },
  checkboxChange: function(event) {
    util.log.checkboxChange(event)
    let totalPay = 0
    event.detail.value.forEach(idx=>{
      totalPay += this.data.records[parseInt(idx)].price
    })
    this.setData({totalPay: util.getMoney(totalPay)})
  },
})
