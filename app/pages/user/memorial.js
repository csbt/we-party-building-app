var app = getApp()
var util = require('../../utils/util')
var audio = require('../../utils/audio')
var api = require('../../utils/webApi')
Page({
  data: {
  },
  onLoad: function(params) {
    util.setThis(this, {globalData: app.globalData})
      .then(()=>params)
      .then(util.log.onLoad)
      .then(d=>{this.setData(d)})
      .then(()=>this.init())
      .then(()=>this.setData({loaded:true}))
      .then(()=>this.onShow())
  },
  onReady: function() {
    util.setThis(this, {ready: true})
      .then(()=>this.onShow())
  },
  onMinuteChange() {
    util.log.onMinuteChange()
    this.update()
  },
  update: function() {
    let now = new Date()
    let year = now.getFullYear()
    let month = now.getMonth() + 1
    let day = now.getDate()
    if(day != this.data.day || month != this.data.month ||  year != this.data.year) {
      util.log.update()
      if(this.data.day) { // 日期切换，重新请求数据
        util.log.dayExists()
        return util.setThis(this,{year,month,day})
          .then(()=>this.init())
      }
      return util.setThis(this, {year,month,day})
        .then(util.log.dateSet)
        .then(()=>util.getElemInfo('members'))
        .then(util.log.members)
        .then(res=>res.height)
        .then(membersHeight=>{
          if(membersHeight) {
            return util.getSystemInfo()
              .then(util.log.getSystemInfo)
              .then(res=>res.windowHeight)
              .then(windowHeight=>{
                const SCROLL_POS = 0.7
                if(membersHeight > windowHeight * SCROLL_POS) {
                  wx.pageScrollTo({scrollTop: membersHeight - windowHeight * SCROLL_POS})
                }
              })
          }
        })
    }
  },
  init: function() {
    return api.get('/memorial')
      .then(util.log.memorial)
      .then(res=>{
        if(res.success) {
          util.setThis(this, {...res.data})
          //util.setThis(this, {members: res.data.passed, passed: res.data.passed}) // debug
            .then(()=>this.update())
        }
      })
  },
  onShow: function() {
    if(this.data.loaded && this.data.ready) {
      util.updateTitle()
      audio.register()
    }
  },
  //update: function() {
  //  for(const i in this.data.users) {
  //    let date = new Date(this.data.users[i].date)
  //    let years = Math.floor((Date.now() - date.getTime() + 60*86400*1000) / (365*86400*1000))
  //    let theDay = new Date(date.getFullYear()+years+date.toISOString().slice(4))
  //    console.log(theDay)
  //    let passed = theDay.toLocaleDateString() < new Date().toLocaleDateString()
  //    this.setData({['users['+i+'].years']: years})
  //    this.setData({['users['+i+'].date']: date.toLocaleDateString()})
  //    this.setData({['users['+i+'].passed']: passed})
  //  }
  //},
})
