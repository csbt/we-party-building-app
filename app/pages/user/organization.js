var app = getApp()
var util = require('../../utils/util')
var api = require('../../utils/webApi')
var audio = require('../../utils/audio')
var customer = require('../../customer')
const SEARCH_DELAY = 2000 // do search on value change after this delay
Page({
  data: {
    titleText: '组织机构',
    path: [0],
    search: {
      inputShowed: false,
      inputVal: "",
      inputChangeAt: 0,
      lastSearch: "",
      pending: false,
      placeHolderOut:"请输入党员姓名/职务/手机号进行搜索",
    },
    page: {
      searchBar: {height: 48, px: true},
      pathBar: {height: 80},
      navBar: {height: 120},
    },
  },
  onReady: function() {
    console.log('onReady', this.isBackground)
    if(!this.data.ready && this.bgPage) {
      //this.setListHeight()
      this.setData({ready: true})
      this.onShow()
    }
  },
  onShow: function() {
    console.log('onShow', this.isBackground)
    if(this.isBackground) {
      if(this.data.loaded) {
        if(this.data.path.length) {
          wx.navigateTo({url:'/'+this.route})
        } else if(getCurrentPages().length > 1) {
          wx.navigateBack()
        } else {
          wx.switchTab('/pages/index/index')
        }
      }
    } else if(this.data.ready && this.data.loaded) {
      audio.register()
      //util.updateTitle()
      util.updatePage()
      //audio.paint()
    }
  },
  onUnload: function(params) {
    if(!this.isBackground) {
      this.bgPage.setData({path: this.data.path.slice(0,-1)})
    }
  },
  onLoad: function(params) {
    // 为实现使用返回键在页面内跳转的功能，一次压入两个相同页面
    let pages = getCurrentPages()
    if(pages.length < 2 || pages[pages.length-2].route != this.route) {
      this.isBackground = true
    } else {
      this.bgPage = pages[pages.length-2]
    }
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
      .then(api.log)
      .then(d=>{this.setData(d)})
      .then(this.init)
      .then(api.log)
      .then(()=>{this.setData({loaded:true})})
      .then(()=>{
        if(this.isBackground) {
          wx.navigateTo({url:'/'+this.route})
        } else {
          this.onReady()
        }
      })
  },
  init: function() {
    if(this.isBackground) {
      return api.get(customer.hierarchyShowBranchOnly ? '/hierarchy?root=branch' : '/hierarchy')
        .then(api.log)
        .then(res=>{
          if(res.success) {
            this.setData(res.data)
          }
        })
    } else {
      this.setData({
        nodes: this.bgPage.data.nodes
      })
      this.update(this.bgPage.data.path)
    }
  },
  setListHeight: function() {
    return util.getSystemInfo()
      .then(api.log)
      .then(res=>{
        // windowHeight = 48px + 80rpx + listHeight + 120rpx
        this.setData({listHeight: res.windowHeight - 200*res.windowWidth/750 - 48})
      })
  },
  myProfile: function() {
    wx.navigateTo({url: './profile?tab=1&id=' + app.globalData.userInfo.appUser.id})
  },
  update: function(path) {
    console.log('update:',path)
    let curNode = this.data
    let label = []
    path.forEach(item=>{
      label.push(curNode.nodes[item].name)
      curNode = curNode.nodes[item]
    })
    this.setData({label, path, curNode})
  },
  tapPath: function(event) {
    console.log(event)
    this.hideInput(true)
    this.update(this.data.path.slice(0,parseInt(event.target.id)+1))
  },
  tapMember: function(event) {
    wx.navigateTo({
      url: './profile?id=' + event.currentTarget.id
    })
  },
  tapNode: function(event) {
    console.log(event)
    let id = parseInt(event.currentTarget.id)
    let dstId = this.data.curNode.nodes[id].id
    let path = this.data.path
    path.push(parseInt(id))
    this.update(path)
  },
  tapDots: function(event) {
    console.log(event)
    wx.showActionSheet({
      itemList: ['拨打电话', '添加到手机通讯录'],
      success: function(res) {
        if (!res.cancel) {
          console.log(res.tapIndex)
          if(res.tapIndex == 0) {
            wx.makePhoneCall({
              phoneNumber: event.target.id
            })
          } else if(res.tapIndex == 1) {
            wx.addPhoneContact({
              firstName: event.target.dataset.name,
              mobilePhoneNumber: event.target.id,
            })
          }
        }
      }
    });
  },

  // 搜索
  checkInput: function() {
    if(!this.data.search.pending 
      && this.data.search.inputVal && this.data.search.inputVal != this.data.search.lastSearch
      && this.data.search.inputChangeAt && this.data.search.inputChangeAt < Date.now()-SEARCH_DELAY 
    ) {
      this.doSearch()
    }
  },
  doSearch: function() {
    let val = this.data.search.inputVal
    if(val != this.data.search.lastSearch) {
      console.log(`doSearch(${val})`)
      this.setData({
        'search.pending': true,
      })
      if(this.data.search.lastSearch) {
        this.update(this.data.path.slice(0,-1))
      }
      if(val) {
        let members = []
        function searchRec(node) {
          members.push(...node.members.filter(member=>{
            if(member.name.indexOf(val) != -1) return true
            if(member.tel && member.tel.indexOf(val) != -1) return true
            if(member.title && member.title.indexOf(val) != -1) return true
          }))
          if(node.nodes) {
            node.nodes.forEach(searchRec)
          }
        }
        searchRec(this.data.curNode)
        this.setData({
          ['label['+this.data.label.length+']']: '搜索结果',
          ['path['+this.data.path.length+']']: 0,
          'curNode.nodes': [],
          'curNode.members': members,
        })
      }
      this.setData({
        'search.lastSearch': val,
        'search.inputChangeAt': Date.now(),
        'search.pending': false
      })
    }
  },
  showInput: function () {
    this.setData({
      'search.inputShowed': true
    });
  },
  hideInput: function (skipSearch) {
    this.setData({
      'search.inputVal': "",
      'search.inputShowed': false
    });
    if(skipSearch) {
      this.setData({'search.lastSearch': ''})
    } else {
      this.doSearch()
    }
  },
  clearInput: function () {
    this.setData({
      'search.inputVal': ""
    });
    this.doSearch()
  },
  inputTyping: function (e) {
    console.log(e.detail.value)
    this.setData({
      'search.inputVal': e.detail.value,
      'search.inputChangeAt': Date.now()
    })
    setTimeout(this.checkInput, SEARCH_DELAY);
  },
})
