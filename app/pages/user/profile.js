var app = getApp()
var api = require('../../utils/webApi')
var audio = require('../../utils/audio')
var util = require('../../utils/util')
Page({
  data: {
    page: {
      topMargin: {height: 220},
    },
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
    .then(api.log)
    .then(d=>{this.setData(d)})
    .then(this.init)
    .then(()=>this.setData({loaded:true}))
    .then(this.onShow)
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.ready && this.data.loaded) {
      audio.register()
      util.updatePage()
    }
  },
  back: function() {
    wx.navigateBack()
  },
  init: function () {
    return api.get('/member?id='+this.data.id)
      .then(api.log)
      .then(res=>{
        if(res.success) {
          res.data.profile = {
            '组织': res.data.orgName,
            '职务': res.data.title || '普通党员',
            '民族': res.data.nation,
            '电话': res.data.tel,
            '出生日期': util.getCnDate(new Date(res.data.birthDate)),
            '入党日期': util.getCnDate(new Date(res.data.joinDate)),
            //'转正日期': util.getCnDate(new Date(res.data.confirmDate)),
          }
          this.setData(res.data)
          this.setData({titleText: (this.data.tab? '我':res.data.name)+'的档案'})
          if(this.data.tab) {
            this.setData({'page.bottomNav': {height: 120}})
          }
        }
      })
      .then(()=>api.get('/score?detail=true&id='+this.data.id))
      .then(api.log)
      .then(res=>{
        if(res.success) {
          this.setData({
            ...res.data,
            'profile.年度积分': res.data.yScore || 0,
            'profile.年度排名': res.data.yRank || '-',
            'profile.本月积分': res.data.score || 0,
            'profile.本月排名': res.data.rank,
          })
        }
      })
  },
})
