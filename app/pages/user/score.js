var app = getApp()
var util = require('../../utils/util')
var api = require('../../utils/webApi')
var audio = require('../../utils/audio')
Page({
  data: {
    titleText: '党员积分档案',
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
    .then(api.log)
    .then(d=>{this.setData(d)})
    .then(this.init)
    .then(()=>this.setData({loaded:true}))
    .then(()=>this.onShow())
  },
  onReady: function() {
    this.setData({ready: true})
    this.onShow()
  },
  onShow: function() {
    if(this.data.ready && this.data.loaded) {
      util.updateTitle()
      audio.register()
    }
  },
  init: function() {
    return Promise.all(
      [
        //app.setTitleText('个人信息'), 
        //app.setTitleColor({frontColor: '#ffffff',backgroundColor: '#4b85f1'})
      ])
      .then(()=>{
        this.setData({
          info: {
            name: app.globalData.userInfo.appUser.name,
            orgName: app.globalData.userInfo.appUser.orgName
          }
        })
      })
      .then(()=>api.get('/score?detail=true'))
      .then(api.log)
      .then((res)=>{
        if(res.success) {
          res.data.detail.forEach(node=>{
            node.displayTime = new Date(node.createdAt).toLocaleString().split(' ')[0]
          })
          this.setData(res.data)
        }
      })
  }
})
