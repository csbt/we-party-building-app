var config = require('../../config')
var util = require('../../utils/util')
var api = require('../../utils/webApi')
var auth = require('../../utils/auth')
var app = getApp()
var customer = require('../../customer')
Page({
  data: {
    titleText: '绑定账号',
    titleColor: {frontColor:customer.verifyTitleColor||'#ffffff', backgroundColor:customer.verifyTitleBgColor},
    name: '',
    tel: '',
  },
  getUserInfo: function(event) {
    auth.updateUserInfo()
  },
  inputName: function(event) {
    util.log.inputName(event)
    this.setData({name: event.detail.value})
  },
  inputTel: function(event) {
    util.log.inputTel(event)
    this.setData({tel: event.detail.value})
  },
  commit: function(event) {
    console.log(event)
    if(event.detail.errMsg != 'getUserInfo:ok') {
      util.log.getUserInfoError(event)
      util.showModal({content: '本程序必须授权头像才可以使用'})
      return
    }
    auth.updateUserInfo(event.detail)
      .then(info=>{
        if(!info.auth) throw '授权失败'
      })
      .then(()=>{
        return api.post(
          {
            url: '/user',
            data: {
              name: this.data.name,
              tel: this.data.tel,
            },
          })
          .then(util.log.commitApi)
          .then(res=>{
            if(res.success) {
              this.setData({name:null,tel:null})
              wx.showToast({
                title: '绑定微信成功',
                icon: 'success',
                duration: 3000,
                success: ()=>{
                  auth.login()
                    .then(util.log.loginSuccess)
                    .then(wx.navigateBack)
                }
              })
            } else {
              this.setData({name:null,tel:null})
              wx.showToast({
                title: res.data||"失败",
                duration: 3000,
                icon: "none",
              })
            }
          })
      })
      .catch(util.error.commit)
      .catch(e=>{
        this.setData({name:null,tel:null})
        util.showModal({content: e})
      })
  },
  onShow: function() {
    util.updateTitle()
  },
  onLoad: function(params) {
    this.setData({globalData: app.globalData})
    Promise.resolve(params)
      .then(api.log)
      .then(d=>{this.setData(d)})
      .then(()=>{this.setData({loaded:true})})
  }
})
