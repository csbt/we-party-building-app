var util = require('./util')
var app
var ctx

const Audio = {
  data: {
    playList: [],
    speaker: 0,
    playing: false,
    inPlay: false,
    hidden: true,
    canAdd: false,
    fontSize: 30,
  },
  audioHandler: function(event) {
    console.log('audioHandler',event.currentTarget.dataset.method,event.target.id)
    console.log('audioHandler',event)
    if(event.currentTarget.dataset.method == 'switchSpeaker') {
      this.switchSpeaker(parseInt(event.detail.value))
    } else {
      this[event.currentTarget.dataset.method](event.currentTarget.id)
    }
  },
  register: function() {
    if(!app || !ctx) {
      this.init()
    }
    let page = util.getPage()
    console.log('register audio in page', page.route)
    if(!page.audioHandler) {
      page.audioHandler = this.audioHandler.bind(this)
    }
    return this.paint()
  },
  getSrc: function(obj) {
    //return `${app.globalData.baseUrl}/audio/${obj.docId}-${obj.curFile}-${app.globalData.speakerId[this.data.speaker]}.mp3`
    return `${app.globalData.cdnBaseUrl}/audio/${obj.docId}-${obj.curFile}-${app.globalData.speakerId[this.data.speaker]}.mp3`
  },
  stop: function() {
    ctx.stop()
    if(this.data.inPlay) {
      this.data.inPlay = false
      this.data.playing = false
    }
    return this.paint()
  },
  switchSpeaker: function(id) {
    console.log('switchSpeaker', id)
    let speaker = parseInt(id)
    if(speaker != this.data.speaker) {
      this.data.speaker = speaker
      this.data.playList.forEach((item,index)=>{
        this.reset(index)
      })
      this.play()
    }
    return this.paint()
  },
  moveTop: function(idx) {
    idx = parseInt(idx)
    if(idx) {
      this.reset(0)
      this.data.playList = [
        this.data.playList[idx], 
        this.data.playList[0],
        ...this.data.playList.slice(1, idx),
        ...this.data.playList.slice(idx+1)
      ]
      this.play()
    }
    return this.paint()
  },
  reset: function(idx) {
    if(idx == 0) this.stop()
    this.data.playList[idx].curFile = 1
  },
  paint: function() {
    let page = util.getPage()
    this.data.canAdd = page.data.audioReady
      && this.data.playList.map(info=>info.docId).indexOf(page.data.id) == -1
    let height = 0
    if(this.data.playList.length) {
      height = this.data.fontSize*1.5;
    } else {
      this.data.hidden = true   // hide play list by default
    }
    page.setData({
      audio: this.data,
      'page.audio': {height},
    })
    util.adjustPageLayout()
    return this
  },
  onCtxEnd: function () {
    console.log('audio.onCtxEnd')
    ctx.canPlay = false
    if(this.data.playList[0].audioFiles > this.data.playList[0].curFile) {
      console.log('next file')
      ++this.data.playList[0].curFile
      ctx.src = this.getSrc(this.data.playList[0])
    } else {
      console.log('end of document')
      if(typeof(this.data.playList[0].onEnd) == 'function') {
        console.log('EOF callback')
        this.data.playList[0].onEnd(this.data.playList[0])
      }
      this.data.playList = this.data.playList.slice(1)
      if(this.data.playList.length) {
        console.log('next document')
        this.play()
      } else {
        console.log('end of list')
        this.data.playing = false
        this.data.inPlay = false
      }
    }
    return this.paint()
  },
  doPlay: function() {
    console.log('audio.doPlay')
    ctx.canPlayThen = null
    ctx.play()
    this.data.playing = true
    this.data.inPlay = true
  },
  play: function() {
    console.log('play', this)
    let src = this.getSrc(this.data.playList[0])
    console.log('new src:', src)
    console.log('old src:', ctx.src)
    if(ctx.src != src) {
      ctx.title = this.data.playList[0].title
      ctx.epname = this.data.playList[0].moduleName + '-' + this.data.playList[0].tabName
      ctx.singer = this.data.playList[0].orgName
      ctx.coverImgUrl = app.globalData.resRoot + this.data.playList[0].cover
      ctx.canPlayThen = this.doPlay.bind(this)
      ctx.src = src
    }
  },
  pause: function() {
    console.log('audio.pause')
    ctx.pause()
    this.data.playing = false
    return this.paint()
  },
  resume: function() {
    console.log('audio.resume')
    ctx.play()
    this.data.playing = true
    return this.paint()
  },
  init: function() {
    console.log('audio init')
    app = getApp()
    ctx = wx.getBackgroundAudioManager()
    ctx.onEnded(this.onCtxEnd.bind(this))
    ctx.onCanplay(this.onCtxReady.bind(this))
    ctx.onError(e=>{console.log('audio error', e)})
    this.data.ctx = ctx
    return this
  },
  toggleHidden: function() {
    this.data.hidden = !this.data.hidden
    return this.paint()
  },
  addThis: function(options) {
    let page = util.getPage()
    return this.addToList({
      docId: page.data.id,
      title: page.data.title,
      orgName: page.data.orgName,
      cover: page.data.cover,
      moduleName: page.data.moduleName,
      tabName: page.data.tabName,
      audioFiles: page.data.audioFiles,
      audioLength: page.data.audioLength,
      ...options,
    })
  },
  playThis: function() {
    while(this.data.playList.length > 1) this.removeDoc(1)
    if(this.data.playList.length) this.removeDoc(0)
    return this.addThis().play()
  },
  removeDoc: function(id) {
    id = parseInt(id)
    this.data.playList = this.data.playList.filter((item,idx)=>idx!=id)
    if(id == 0) {
      ctx.stop()
      console.log(this)
      if(this.data.playList.length) {
        if(this.data.playing) {
          this.play()
        } else {
          this.data.inPlay = false
        }
      } else {
        this.data.playing = false
        this.data.inPlay = false
      }
    }
    return this.paint()
  },
  addToList: function(obj) {
    console.log('audio add:', obj)
    obj.curFile = 1
    this.data.playList.push(obj)
    console.log('addToList', this)
    return this.paint()
  },
  onCtxReady: function() {
    console.log('audio ready:', ctx.src)
    ctx.canPlay = true
    if(ctx.canPlayThen && typeof(ctx.canPlayThen == 'function')) {
      ctx.canPlayThen(ctx)
    }
    this.paint()
  },
}
module.exports = Audio
