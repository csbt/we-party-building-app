var config = require('../config')
const sessionKey = 'weapp_session_' + config.consts.WX_SESSION_MAGIC_ID
var util = require('./util')
var api = require('./webApi')
var socket = require('./socket')
function updateUserInfo(info) {
  let app = getApp()
  return new Promise((resolve, reject) => {
    // 服务器已有用户数据
    if(app.globalData.userInfo.auth) {
      resolve(app.globalData.userInfo)
    } else if(info) {  // 带参数调用
      util.log.updateUserInfo(info)
      app.globalData.userInfo.auth = true // 更新全局变量
      app.globalData.userInfo.avatarUrl = info.userInfo.avatarUrl
      getCurrentPages().forEach(page=>{   // 更新页面
        page.setData({
          'globalData.userInfo.auth': true,
          'globalData.userInfo.avatarUrl': info.userInfo.avatarUrl,
        })
      })
      resolve(app.globalData.userInfo)
    } else { // 检查授权
      wx.getSetting({
        success: settings=>{
          util.log.getSetting(settings)
          if(settings.authSetting['scope.userInfo']) {  // 用户已授权
            wx.getUserInfo({
              withCredentials: false,
              success: info=>{
                util.log.getUserInfo(info)
                api.post({  // 上传用户数据
                  url: '/user',
                  data: {
                    userInfo: info.userInfo,
                  }
                })
                app.globalData.userInfo.auth = true // 更新全局变量
                app.globalData.userInfo.avatarUrl = info.userInfo.avatarUrl
                getCurrentPages().forEach(page=>{   // 更新页面
                  page.setData({
                    'globalData.userInfo.auth': true,
                    'globalData.userInfo.avatarUrl': info.userInfo.avatarUrl,
                  })
                })
                resolve(app.globalData.userInfo)
              }
            })
          } else {
            resolve(app.globalData.userInfo)
          }
        },
        fail: reject,
      })
    }
  })
}
function login() {
  // none -> <login> -> code -> <loginUrl> -> skey, userinfo
  // skey -> <requestUrl> -> userinfo
  let skey = wx.getStorageSync(sessionKey).skey
  return new Promise((resolve, reject)=>{
    if(skey) {                                    // 登陆过
      wx.checkSession({
        success: res=>{                           // 微信登陆未过期
          util.log.checkSessionSuccess(res)
          wx.request({                    // 请求用户信息
            url: config.service.requestUrl,
            header: {[config.consts.WX_HEADER_SKEY.toLowerCase()]: skey},
            success: resolve,             // 得到用户信息
            fail: (err) => {
              util.log.userFail(err)
              resolve()                   // 请求出错，重新登陆
            }
          })
        },
        fail: (e)=>{
          util.log.checkSessionFail(e)
          resolve()
        }
      })
    } else {
      resolve()
    }
  })
    .then(res=>{
      if(res && res.data.code == 0) return res
      return new Promise((resolve, reject) => {
        wx.login({                              // 重新登陆
          success: loginResult=>{
            util.log.loginSuccess(loginResult)
            //wx.request({
            //  url: config.service.loginUrl,
            //  header: {
            //    [config.consts.WX_HEADER_CODE]: loginResult.code,
            //    'x-wx-appid': config.appid,
            //    'xxxx': 'xxxx',
            //  },
            //  success: resolve,
            //  fail: util.error.requestLogin,
            //})
            let obj = {
              url: config.service.loginUrl,
              header: {
                [config.consts.WX_HEADER_CODE]: loginResult.code,
                'X-WX-Appid': config.appid,
              },
              success: resolve,
              fail: util.error.requestLogin,
            }
            util.log.obj(obj)
            wx.request(obj)
          },
          fail: util.error.login,
        })
      })
    })
    .then(util.log.login)
    .then(res=>res.data.data)
    .then(util.log.wxLogin)
    .then(res=>{
      if(!res.skey) throw '没有skey'
      // 更新cache
      if(res.skey != skey) {
        util.log.updateSkey(res.skey)
        wx.setStorage({
          key: sessionKey,
          data: {skey: res.skey},
        })
      }
      // 是否绑定
      let logged = !!res.appUser
      if(logged) {
        res.appUser.partyStanding = util.getPartyStanding(res.appUser.joinDate)
        socket.init()
      }

      let app = getApp()
      app.globalData.userInfo = {
        ...res.userinfo,
        appUser: res.appUser,
      }
      app.globalData.logged = logged
      app.globalData.logFinish = true
      updateUserInfo()
      let page = util.getPage()
      if(page && page.onEndLogin && typeof(page.onEndLogin) == 'function') {
        return page.onEndLogin(logged)
      } else {
        return logged
      }
    })
}
module.exports = {
  login,
  updateUserInfo,
}
