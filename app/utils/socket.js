var util = require('./util')
var api = require('./webApi')
var config = require('../config')
var onOpen = []
var io = require('./weapp.socket.io')
var socket
const wxConsts = require('../vendor/wafer2-client-sdk/lib/constants')
const sessionKey = 'weapp_session_' + wxConsts.WX_SESSION_MAGIC_ID
const ROUTES = {
  'stream': ['pages/content/stream', 'pages/content/meeting'],
}
function notifyHandler(data) {
  let route = ROUTES[data.path]
  let resolved = 0
  getCurrentPages().forEach(page=>{
    if(route.indexOf(page.route) != -1 && page.data.id == data.id && page.onNotify) {
      if(resolved) util.log.multipleNotify(resolved)
      page.onNotify(data)
      ++resolved
    }
  })
  if(!resolved) util.log.discarded(data)
}
function init(obj) {
  return new Promise((resolve, reject) => {
    //socket = io(config.service.host, {
    socket = io(config.service.socketUrl, {
      path: config.service.socketPrefix + '/socket.io',
      query: {[wxConsts.WX_HEADER_SKEY.toLowerCase()]: wx.getStorageSync(sessionKey).skey},
      reconnection: true,
      reconnectionDelay: 1000,
      reconnectionDelayMax : 5000,
      reconnectionAttempts: 99999,
    })
    socket.on('connect', resolve)
    socket.on('disconnect', util.log.socketDisconnect)
    socket.on('notify', notifyHandler)
  })
}
function request(obj) {
  return new Promise((resolve, reject)=>{
    let timestamp = Date.now().toString()
    util.log.socket_tx({[obj.action]: obj.data})
    socket.emit('request', {
      ...obj,
      timestamp,
    })
    socket.once(timestamp, resolve)
  })
    .then(util.log.socket_rx)
}

function emit() {
}

function send(message) {
  return request({
    action: 'message',
    data: message
  })
}
var requestAction = {
  get(target, action) {
    return data=>{
      return request({
        path: 'stream',
        action,
        data,
      })
    }
  }
}
var stream = new Proxy({}, requestAction)

module.exports = {
  init,
  send,
  stream,
}

