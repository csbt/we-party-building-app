const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const MINUTE = 60*1000
const HOUR = 60 * MINUTE
const DAY = 24 * HOUR
const MONTH = 30 * DAY

var getCnDate = date=>{
  date = new Date(date)
  return `${date.getFullYear()}年${date.getMonth()+1}月${date.getDate()}日`
}
var getCnTime = date=>{
  let hours = date.getHours()
  let minutes = date.getMinutes()
  return `${getCnDate(date)} ${hours>12?"下午"+(hours-12):"上午"+hours}点${minutes?minutes+"分":"整"}`
}
var getMoney = number=>{
  let fixed = Math.floor(number)
  let frag = Math.floor((number - fixed) * 100)
  return fixed + '.' + formatNumber(frag)
}

var showOldDate = dateStr=>{
  let date = new Date(dateStr)
  let delta = Date.now() - date.getTime()
  if(delta < MINUTE) return '刚刚'
  if(delta < HOUR) return Math.floor(delta/MINUTE).toString() + '分钟前'
  if(delta < DAY) return Math.floor(delta/HOUR).toString() + '小时前'
  if(delta < MONTH) return Math.floor(delta/DAY).toString() + '天前'
  return date.getFullYear().toString()+'年'+(date.getMonth()+1).toString()+'月'
}
var getPartyStanding = joinDate => {
  joinDate = new Date(joinDate)
  let now = new Date()
  let years = now.getYear() - joinDate.getYear()
  let months = now.getMonth() - joinDate.getMonth()
  let days = now.getDate() - joinDate.getDate()
  if(months < 0 || (months == 0 && days < 0)) {
    --years
    months += 12
  }
  if(years) return years + '年'
  if(days < 0) {
    --months
  }
  if(months) return months + '个月'
  return Math.max(1, Math.floor((now - joinDate) / 86400000)) + '天'
}
const SECONDS_PER_DAY = 86400
const SECONDS_PER_HOUR = 3600
const SECONDS_PER_MINUTE = 60
var formatSeconds = sec => {
  let day = Math.floor(sec / SECONDS_PER_DAY)
  sec %= SECONDS_PER_DAY
  let hour = Math.floor(sec / SECONDS_PER_HOUR)
  sec %= SECONDS_PER_HOUR
  let minute = Math.floor(sec / SECONDS_PER_MINUTE)
  sec %= SECONDS_PER_MINUTE
  if(day) {
    return day + '天' + hour + '小时' + minute + '分' + sec + '秒'
  } else if(hour) {
    return hour + '小时' + minute + '分' + sec + '秒'
  } else if(minute) {
    return minute + '分' + sec + '秒'
  } else {
    return sec + '秒'
  }
}

// 显示繁忙提示
var showBusy = text => wx.showToast({
    title: text,
    icon: 'loading',
    duration: 10000
})

// 显示成功提示
var showSuccess = text => wx.showToast({
    title: text,
    icon: 'success'
})

// 显示失败提示
var showModal = obj => {
  wx.hideToast()
  return new Promise((resolve, reject) => {
    wx.showModal({
      ...obj,
      showCancel: false,
      success: resolve,
      fail: reject,
    })
  })
}

var setTitleText = (title) => {
  return new Promise((resolve, reject) => {
    wx.setNavigationBarTitle({
      title,
      success: resolve,
      fail: reject
    })
  })
}
var setTitleColor = (options) => {
  return new Promise((resolve, reject) => {
    wx.setNavigationBarColor({
      ...options,
      success: resolve,
      fail: reject
    })
  })
}
var getSystemInfo = () => {
  return new Promise((resolve, reject) => {
    wx.getSystemInfo({
      success: resolve,
      fail: reject,
    })
  })
}
var getPage = () => {
  let pages = getCurrentPages()
  return pages.length ? pages[pages.length-1] : null
}
var getPrevPage = () => {
  let pages = getCurrentPages()
  return pages.length > 1 ? pages[pages.length-2] : null
}
var updateTitle = (page) => {
  if(!page) page = getPage()
  if(page) {
    return Promise.all(
      [
        page.data.titleText != null ? setTitleText(page.data.titleText) : Promise.resolve(),
        page.data.titleColor? setTitleColor(page.data.titleColor) : Promise.resolve(),
      ])
      .then(()=>page)
  }
}
var adjustPageLayout = (page) => {
  if(!page) {
    page = getPage()
  }
  if(page && page.data.page) {
    return getSystemInfo()
      .then(res=>{
        let listHeight = res.windowHeight // px
        let rpx = res.windowWidth / 750
        //for(const elem of page.data.page) {
        //console.log(listHeight)
        //console.log(page.data.page)
        for(const key in page.data.page) {
          let elem = page.data.page[key]
          listHeight -= elem.px ? elem.height : elem.height * rpx
        }
        return listHeight
      })
      .then(listHeight=>{page.setData({listHeight})})
      .then(()=>page)
  }
}
var updatePage = () => {
  return Promise.resolve(getPage())
    .then(updateTitle)
    .then(adjustPageLayout)
}
var setThis = (page, data) => { // this.setData, promisified
  return new Promise((resolve, reject) => {
    page.setData(data, ()=>resolve(page.data))
  })
}
var setData = (data) => setThis(getPage(), data)
var msToClock = (time) => {
  time /= 1000
  let hour = Math.floor(time/3600)
  time %= 3600
  let minute = Math.floor(time/60)
  time %= 60
  time = Math.floor(time)
  return formatNumber(hour) + ':' + formatNumber(minute) + ':' + formatNumber(time)
}
var logWithTag = {
  get(target, tag) {
    return d=>{
      console.log(tag,':', d)
      return d
    }
  }
}
var log = new Proxy({}, logWithTag)

var errorWithTag = {
  get(target, tag) {
    return e=>{
      console.error(tag,':', e)
      throw e
    }
  }
}
var error = new Proxy({}, errorWithTag)  

function compareVersion(v1, v2) {
  v1 = v1.split('.')
  v2 = v2.split('.')
  var len = Math.max(v1.length, v2.length)

  while (v1.length < len) {
    v1.push('0')
  }
  while (v2.length < len) {
    v2.push('0')
  }

  for (var i = 0; i < len; i++) {
    var num1 = parseInt(v1[i])
    var num2 = parseInt(v2[i])

    if (num1 > num2) {
      return 1
    } else if (num1 < num2) {
      return -1
    }
  }
  return 0
}
function versionGt(ver) {
  return compareVersion(wx.getSystemInfoSync().SDKVersion, ver) >= 0
}
function scanCode(expectedPath) {
  return new Promise((resolve, reject)=>{
    wx.scanCode({
      onlyFromCamera: true,
      success: res=>resolve(res),
      fail: res=>reject(res),
    })
  })
  .then(log.utilScanCode)
  .then(res=>{
    if(res.errMsg != "scanCode:ok") throw '扫码失败'
    if(expectedPath && res.path != expectedPath) throw '非本活动/会议码'
    return res
  })
}
function scan() {
}

function getElemInfo(id) {
  return new Promise((resolve, reject) => {
    wx.createSelectorQuery().select('#'+id).boundingClientRect(resolve).exec()
  })
}
module.exports = {
  formatNumber,
  formatTime,
  getCnDate,
  getCnTime,
  getMoney,
  showBusy,
  showSuccess,
  showModal,
  showOldDate,
  updateTitle,
  getPage,
  getPrevPage,
  getSystemInfo,
  adjustPageLayout,
  updatePage,
  formatSeconds,
  setThis,  // setThis(page, data).then(cb): page.setData(data, cb)
  setData,  // setData(data).then(cb): currentFrontPage.setData(data, cb)
  msToClock,
  log,      // log.xxx(msg): (msg)=>{console.log('xxx:', msg); return msg}
  error,    // error.xxx(msg): (msg)=>{console.error('xxx:', msg); raise msg}
  compareVersion,
  versionGt,
  getPartyStanding,
  scanCode,
  getElemInfo
}
