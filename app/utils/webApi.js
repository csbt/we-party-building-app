var config = require('../config')
var util = require('./util')
const DOW = '日一二三四五六'
const sessionKey = 'weapp_session_' + config.consts.WX_SESSION_MAGIC_ID
function getHeader() {
  return {[config.consts.WX_HEADER_SKEY.toLowerCase()]: wx.getStorageSync(sessionKey).skey}
}
function assertSuccess(res) {
  if(res.success) return res.data
  throw res.data
}

function log(data) {
  console.log(data)
  return data
}
function error(err) {
  console.error(err)
  throw err
}
function getStatus(doc) {
  switch(doc.status) {
    case 'PRESENT'  : return {status: '已参会', class: 'present'}
    case 'READ'     : return {status: '云参会', class: 'read'}
    case 'VACANT'   : return {status: '已请假', class: 'vacant'}
    case 'ABSENT'   : return {status: '已缺席', class: 'absent'}
    case 'JOIN'     : return {status: '待确认', class: 'join'}
    case 'SCHEDULED':
      let now = Date.now()
      if(now < (new Date(doc.startTime)).getTime() - 30*60*1000) 
        return {status: '筹备中', class: 'preparing'}
      if(now < (new Date(doc.startTime)).getTime())
        return {status: '签到中', class: 'registering'}
      if(now < (new Date(doc.endTime)).getTime())
        return {status: '进行中', class: 'going'}
      if(now < (new Date(doc.allowReadAfter)).getTime())
        return {status: '整理中', class: 'sum'}
      if(now > (new Date(doc.allowReadBefore)).getTime())
        return {status: '已过期', class: 'expired'}
      return {status: '可参会', class: 'ready'}
    default: return {status: doc.status||"出错了", class: 'error'}
  }
}

var postTools = {
  'meetingListItem' : doc=>{
    let startDate = new Date(doc.startTime)
    let dateStr = startDate.toLocaleString('zh-CN', {timeZone:'Asia/Shanghai'})
    let hourStart = dateStr.indexOf(' ') + 1
    return {
      ...doc,
      ...getStatus(doc),
      year: startDate.getFullYear()%100,
      month: util.formatNumber(startDate.getMonth()+1),
      date: util.formatNumber(startDate.getDate()),
      day: DOW[startDate.getDay()],
      passed: startDate.getTime() < Date.now(),
      time: dateStr.slice(hourStart,-3)
    }
  },
}

function postProcess(data) {
  let result = data.postProcess && (data.postProcess in postTools)
    ? postTools[data.postProcess](data) : data
  if('nodes' in data) {
    result.nodes = data.nodes.map(postProcess)
  }
  return result
}

function save(key, data, expiration) {
  let expireAt = Date.now() + (expiration || 48*3600*1000)
  wx.setStorage({
    key,
    data:{data,expireAt},
    fail: doGC,
  })
}

function getStorageAlive(key) {
  return new Promise((resolve, reject) => {
    wx.getStorage({
      key,
      fail: reject,
      success: res=>{
        if(res.data.expireAt < Date.now()) {
          wx.removeStorage({key}) // async
          reject('expired')
        } else {
          resolve(res.data.data)
        }
      },
    })
  })
}
var load = getStorageAlive
function doGC() {
  wx.getStorageInfo({
    success: res=>{
      Promise.all(res.keys.map(getStorageAlive))
        .catch()
        .then(()=>{console.log('user gc')})
    },
  })
}

function request(obj) {
  return new Promise((resolve, reject) => {
    wx.request({
      ...obj,
      header: getHeader(),
      success: resolve,
      fail: reject,
    })
  })
}

function getWithAuth(obj) {
  return new Promise((resolve, reject)=>{
    wx.request({
      ...obj,
      header: getHeader(),
      //login: true,
      success: res=>{
        //util.log.get(res)
        //util.log.delta(new Date() - new Date(res.header.date||res.header.Date))
        //resolve(res.data.data)
        resolve(res)
      }
    })
  })
}

function postWithAuth(obj) {
  return new Promise((resolve, reject)=>{
    wx.request({
      ...obj,
      header: getHeader(),
      method: 'POST',
      //login: true,
      success: res=>{
        resolve(res.data.data)
        util.log.post(res)
        util.log.delta(new Date() - new Date(res.header.date||res.header.Date))
      }
    })
  })
}
function requestWithAuth(url) {
  return getWithAuth({url}).then(res=>res.data.data)
}
function get(path) {
  return requestWithAuth(config.service.apiUrl+path)
}
function post(obj) {
  obj.url = config.service.apiUrl + obj.url
  return postWithAuth(obj)
}
function getWithData(obj) {
  obj.url = config.service.apiUrl + obj.url
  return getWithAuth(obj).then(res=>res.data.data)
}
function listMeeting(id) {
  return get('/meeting')
    .then(assertSuccess)
    .then(postProcess)
}
function listColumnWithQuery(query) {
  return getWithData({url: '/column', data: query})
    .then(assertSuccess)
    .then(postProcess)
}
function parseRes(res) {
  result = {}
  if(res&&res.success) {
    result.nodes = res.data.map(doc=>{
      return {
        ...doc,
        timestamp: new Date(doc.startTime),
        ...getStatus(doc),
      }
    })
  }
  return result
}
function requirePage(path) {
  return requestWithAuth(config.service.pageUrl+path)
}
function requireRes(path) {
  return requestWithAuth(config.service.resourceUrl+path)
}
function requireDoc(id) {
  return requestWithAuth(config.service.documentUrl+'/'+id)
}
function requireCom(id) {
  return requestWithAuth(config.service.commentUrl+'/'+id)
}

const ASCII_A = 65
function parseAnswer(answer, type, optionNumber) {
  if(type == 'SINGLE') {
    return answer && (answer.charCodeAt() - ASCII_A)
  } else if(type == 'MULTI') {
    let ret = new Array(optionNumber).fill(false)
    for(const c in answer) {
      ret[answer[c].charCodeAt() - ASCII_A] = true
    }
    return ret
  } else {
    throw '不支持的答案类型' + type
  }
}
function genAnswer(ori) {
  if(typeof(ori) == 'number') {
    return String.fromCharCode(ASCII_A+ori)
  } else if(typeof(ori) == 'string') {
    return String.fromCharCode(ASCII_A+parseInt(ori))
  } else if(ori === null || ori.length == 0) {
    return null
  } else {
    try{
    return ori.map((val,idx)=>val?String.fromCharCode(ASCII_A+idx):'').join('')
    }catch(e){console.log('error',typeof(ori), ori)}
  }
}
function postProcessExam(exam) {
  exam.startTime = new Date(exam.startTime)
  exam.endTime = new Date(exam.endTime)
  let now = new Date()
  exam.displayStartTime = util.getCnTime(exam.startTime)
  exam.displayEndTime = util.getCnTime(exam.endTime)
  exam.tab = 0
  if(exam.startAt) {
    if(exam.finished) {
      exam.tab = 1
      exam.scoreDisplay = Math.floor(exam.score *100/ exam.numQuestions + 0.5)
      if(exam.score >= exam.numPass) {
        exam.status = 'passed'
      } else {
        exam.status = 'failed'
      }
    } else {
      exam.status = 'running'
    }
  } else {
    if(now > exam.endTime) {
      exam.status = 'expired'
    } else if(now < exam.startTime) {
      exam.status = 'future'
    } else {
      exam.status = 'open'
    }
  }
  if(exam.questions) {
    exam.questions.forEach(question=>{
      question.options = JSON.parse(question.options)
    })
    if(exam.answers) {
      exam.answersOri = JSON.parse(exam.answers)
    } else {
      exam.answersOri = new Array(exam.numQuestions).fill(null)
    }
    exam.answers = exam.answersOri.map((answer,idx)=>{
      return parseAnswer(answer, exam.questions[idx].type, exam.questions[idx].options.length)
    })
    if(exam.timeUsed) {
      exam.timeUsed = util.formatSeconds(exam.timeUsed)
    }
  }
  if(exam.totalUsers) {
    exam.betterThan = Math.floor((exam.totalUsers - exam.betterUsers - 1) * 100 / (exam.totalUsers - 1) + 0.5)
  }
  return exam
}
function listExams() {
  return get('/exam')
    .then(assertSuccess)
    .then(res=>res.map(postProcessExam))
}
function getExam(id) {
  return get('/exam?id='+id)
    .then(util.log.getExam)
    .then(assertSuccess)
    .then(postProcessExam)
}
function startExam(id) {
  return getWithData(
    {
      url: '/exam',
      data: {
        id: id,
        start: true,
      },
    })
    .then(assertSuccess)
    .then(postProcessExam)
}
function postExam(data) {
  return post(
    {
      url: '/exam',
      data: {
        recordId: data.recordId,
        answers: data.answers.map(genAnswer)
      }
    })
    .then(assertSuccess)
    .then(postProcessExam)
}

var Session = require('../vendor/wafer2-client-sdk/lib/session')

function upload(filePath) {
  return new Promise((resolve,reject) => {
    wx.uploadFile({
      url: config.service.uploadUrl,
      filePath: filePath,
      name: 'file',
      header: {'X-WX-SKey': Session.get().skey},
      success: res=>{
        resolve(JSON.parse(res.data).data)
      },
      fail: reject,
    })
  })
}
function chooseImage() {
  return new Promise((resolve, reject) => {
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: res=>{
        resolve(res.tempFilePaths[0])
      }
    })
  })
}
module.exports = {
  log,
  error,
  get,
  post,
  requirePage,
  listColumnWithQuery,
  listMeeting,
  requireRes,
  requireDoc,
  requireCom,
  parseRes,
  upload,
  chooseImage,
  listExams,
  getExam,
  startExam,
  postExam,
  request,
  getWithAuth,
  getWithData,
}
