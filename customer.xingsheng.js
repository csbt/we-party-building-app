module.exports = {
  profile: "xingsheng",
  titleText: "兴胜智慧党建",
  appId: "wx969cb5c33ec5a799",
  animation: "static",
  animationTemplate: "xingsheng",
  animationColor: "#f9edd7",
  animationTitleColor: "#f6f6f6",
  animationSubtitle: "开启党员教育管理新模式",
  animationImageTop: "/resources/xingsheng/xingsheng_top.png",
  animationImageBottom: "/resources/xingsheng/xingsheng_bottom.png",
  animationFooter1: "中共兴胜镇委员会",
  verifyTitleColor: "#000000",
  verifyTitleBgColor: "#f6f6f6",
  imageMeetingTabBg: "/meeting/meeting-xingsheng.jpg",

}
