module.exports = {
  profile: "xueyuan",
  titleText: "学院党建云",
  appId: "wxd5fac7f2bf604f3d",
  animation: "static",
  animationColor: "#ffa914",
  animationTemplate: "xueyuan",
  animationSubtitle: "开启党建新时代",
  animationImageCenter: "/icon/start-animation/xueyuan_center.jpg",
  animationImageBottom: "/icon/start-animation/xueyuan_bottom.jpg",
  animationFooter1: "自由路街道党工委",
  animationFooter2: "自由路街道学院社区党委",
}
