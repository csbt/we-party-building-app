#!/usr/bin/env sh
if ! [ $1 ]; then
  echo USAGE: $0 PROFILE
  exit 1
fi
if ! [ -e customer.$1.js ]; then
  echo ERROR: profile $1 not found
  exit 2
fi
( set -e
cp customer.$1.js app/customer.js
cp app.$1.json app/app.json
rm -rf app/resources/*
cp -r resources/$1 app/resources/
echo profile $1 migrated successfully
)
